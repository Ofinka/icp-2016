# Projekt:	ICP 2016-Othello
# Autor:	Karel Pokorny
# Email:	xpokor68@stud.fit.vutbr.cz


all: cli gui
	

cli:
	g++ -std=c++11 src/AI.cpp src/game.cpp src/main_cli.cpp src/tools.cpp src/run_cli.cpp -o hra2016-cli

gui:
	cd src/; qmake hra2016.pro; $(MAKE) $(MFLAGS); mv hra2016 ../
    
doxygen: 
	rm -rf doc/
	doxygen Doxyfile

run: run-gui run-cli

run-gui:
	./hra2016 &

run-cli:
	./hra2016-cli

pack:
	mkdir xpokor68
	cp -rf src/ saved/ doc/ examples/ Makefile Doxyfile README.txt xpokor68/
	tar -pczf xpokor68.tar.gz xpokor68
	rm -rf xpokor68

clean:
	rm -f hra2016-cli
	rm -f src/*.o;
	rm -f hra2016
	rm -f src/Makefile
	rm -rf doc/*
