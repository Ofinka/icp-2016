####################################################################################

Autor: Karel Pokorny, xpokor68
Datum: 8.5.2016

Toto je skolni projekt do predmetu Seminar C++ (ICP) na FIT VUT.

Jedna se o implementaci aplikace Othello (Reversi), ktera vychazi ze stejnojmenne deskove hry.

Aplikace krome pravidel hry implementuje moznosti jako:
- ulozit rozehranou hru
- nacist rozehranou hru
- hrat vice her zaroven
- pohybovat se a prepsat historii tahu
- zvolit si herni mod (single/multi player)
- zvolit si jednu ze dvou obtiznosti
- zvolit si velikost hraci desky [6,8,10,12]

Aplikace obsahuje CLI i GUI verzi.

Zadani: https://www.fit.vutbr.cz/study/courses/ICP/public/ICP-PRJ-zadani.html.cs

####################################################################################

