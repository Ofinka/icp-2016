/**
 *  @file AI.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Implementace single player algoritmu.
 *
 */

#include "AI.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdbool.h>
#include <limits>
#include <iterator>
#include <unistd.h>

using namespace std;

extern int idHrace;
extern int errorIndicate;

/*! Algoritmus zjisti vsechna mozna pole kam muze polozit kamen a nahodne jedno vybere
 */
void randomStrategyEnemy(Game* hra)
{
    //sleep(1);
    multimap<int, int> *legalMoves = new multimap<int, int>();
    hra->checkDirections(legalMoves, 'W');
    
    if (legalMoves->begin() == legalMoves->end())
    {
        idHrace++;
        return;
    }
    
    // vyberu nahodne cislo z mapu a polozim na desku
    multimap<int, int>::iterator item = legalMoves->begin();
    int random_index = rand() % legalMoves->size();
    std::advance(item, random_index);

    hra->getEnemyTables(item->first, item->second, 'W', NULL, false);
    idHrace++;
}

/*! Funkce zjisti pro kazdy tah jaky vysledek by mu prinesl
 * @return Vraci pocet vlastnich kamenu na plose po provedeni tahu
 */
int testScore(Game* hra, int radek, int sloupec)
{
    int x = 0;
    
    hra->getEnemyTables(radek, sloupec, 'W', &x, true);

    return x;
}

/*! Algoritmus ktery vybere takovy tah, ktery mu prinese nejvetsi okamzity zisk
*/
void greatestProfitAlg(Game* hra)
{
    //sleep(1);
    multimap<int, int> *legalMoves = new multimap<int, int>();
    hra->checkDirections(legalMoves, 'W');
    
    if (legalMoves->begin() == legalMoves->end())
    {
        idHrace++;
        return;
    }
    
    int bestRadek, bestSloupec;
    int bestResult = 0;
    int tmpResult;
    multimap<int, int>::iterator item = legalMoves->begin();
    
    while(item != legalMoves->end())
    { 
            
        tmpResult = testScore(hra, item->first, item->second);

        if (bestResult < tmpResult)
        {
            bestResult = tmpResult;
            bestRadek = item->first;
            bestSloupec = item->second;
        }
        advance(item, 1);
    }
    hra->getEnemyTables(bestRadek, bestSloupec, 'W', NULL, false);
    idHrace++;
}
