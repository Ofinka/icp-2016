/**
 *  @file AI.h
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Deklarace funkci pro single player algoritmy
 *
 */

#ifndef AI_H
#define AI_H

#include "tools.h"
#include "game.h"


void randomStrategyEnemy(Game* hra);
int testScore(Game* hra, int radek, int sloupec);
void greatestProfitAlg(Game* hra);

#endif
