/**
 *  @file cli.h
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Hlavni header soubor pro CLI verzi.
 *
 */

#ifndef CLI_H
#define CLI_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdbool.h>
#include <limits>
#include <iterator>
#include <unistd.h>

#include "tools.h"
#include "game.h"
#include "shared.h"
#include "AI.h"

void handleErr(int error);

/**
 * Seznam prikazu ktere mohou byt zadany pri CLI verzi programu.
 */
enum commands
{
    PREW    = 20,
    NEXT,
    MENU,
    EXIT,
    RETURN,
    SAVE,
    SKIP,
    LOAD
};
int playTimeCommands(const std::string &tah);
void playGame(Game* hra, char uroven);

#endif
