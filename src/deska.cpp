/**
 *  @file deska.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Implementacni detaily metod tridy deska
 *
 */

#include "deska.h"
#include "ui_deska.h"
#include "mainwindow.h"
#include <QLabel>
#include <QGridLayout>
#include <iostream>
#include <QColorDialog>
#include <QMessageBox>

extern int velikostPole;
extern char mod;
// pocitadlo na rozliseni ktery hrac je na tahu
int hrac = 1;
extern int nahrana;

// pocet obsazenych kamenu
int B;
int W;

void deska::initColor()
{
    hrac = 1;
    QPushButton* button = new QPushButton;

    for (int i = 0; i < velikostPole; ++i)
    {
        for (int j = 0; j < velikostPole; ++j)
        {
            button = getButton(i, j);
            button->setStyleSheet("background-color: #778f9d");
        }
    }
    ui->pushButton->setStyleSheet("background-color: #ca2723");
    ui->pushButton_2->setStyleSheet("background-color: #ca2723");
    ui->pushButton_3->setStyleSheet("background-color: #ca2723");
    ui->pushButton_4->setStyleSheet("background-color: #ca2723");
    ui->pushButton_5->setStyleSheet("background-color: #ca2723");
    ui->pushButton_6->setStyleSheet("background-color: #ca2723");

    ui->textBrowser->setStyleSheet("background-color: white");
    ui->textBrowser_2->setStyleSheet("background-color: white");
}

void deska::printPlayer()
{
    if (hrac % 2 == 1)
        ui->label->setText("<h3>Černý hráč je na tahu</h>");
    else
        ui->label->setText("<h3>Bílý hráč je na tahu</h>");
}

void deska::playGame()
{
    printPlayer();

    // lehka obtiznost
    if (mod == 'E' && (hrac%2 == 0))
    {
        randomStrategyEnemy(_hra);
        arrToButton(_herniDeska);
        hrac++;
        if (this->gameEnd())
        {
            QMessageBox* msg = new QMessageBox();
            msg->setText("<h2><b><u>KONEC HRY</u></b></h>");
            if (B > W)
                msg->setInformativeText("Černý hrac vyhral!");
            else
                msg->setInformativeText("Bílý hrac vyhral!");
            msg->show();
        }

        playGame();
    }
    // stredni obtiznost
    else if (mod == 'S' && (hrac%2 == 0))
    {
        greatestProfitAlg(_hra);
        arrToButton(_herniDeska);
        hrac++;
        if (this->gameEnd())
        {
            QMessageBox* msg = new QMessageBox();
            msg->setText("<h2><b><u>KONEC HRY</u></b></h>");
            if (B > W)
                msg->setInformativeText("Cerny hrac vyhral!");
            else
                msg->setInformativeText("Bily hrac vyhral!");
            msg->show();
        }

        playGame();
    }
    // multiplayer
    else
    {
        _legalMoves = _hra->getLegalMoves(&hrac);
        QPushButton *button;

        if (_legalMoves->begin() == _legalMoves->end())
            return;

        //signalizace moznych tahu
        for(auto x: (*_legalMoves))
        {
            button = getButton(x.first, x.second);
            button->setStyleSheet("background-color: #0aa05c");
        }
        printScores();
    }
}

bool deska::validMoves(int a, int b, std::multimap<int, int>* moves)
{
    // zadny mozny tah
    if (moves->begin() == moves->end())
        return false;

    for(auto x: (*moves))
        if (x.first != a || x.second != b)
            continue;
        else
            return true;
    return false;
}

deska::deska(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::deska)
{
    ui->setupUi(this);
    this->setWindowTitle("hra2016");
    this->setStyleSheet("background-color: #a88465");
    this->initColor();

    deskaSetup();
    _herniDeska = _hra->getGameBoard();
    arrToButton(_herniDeska);

    playGame();
}

// specialne kvuli nahrani existujici hry
deska::deska(Game* hra):
    ui(new Ui::deska)
{
    ui->setupUi(this);
    this->setWindowTitle("hra2016");
    this->setStyleSheet("background-color: #a88465");
    this->initColor();

    // potreba nahrat jiz existujici historii tahu 
    this->_hra = hra;
    _herniDeska = _hra->getGameBoard();
    arrToButton(_herniDeska);

    playGame();
}

void deska::deskaSetup()
{
    char** arr = makeArray(velikostPole);
    this->_hra = new Game(arr, velikostPole);
}

deska::~deska()
{
    delete ui;
}

// zpet na menu
void deska::on_pushButton_5_clicked()
{
    MainWindow* menu = new MainWindow;
    menu->show();
    this->hide();
}

void deska::arrToButton(char** _gameBoard)
{
    QPushButton* button;

    for (int i = 0; i < velikostPole; ++i)
    {
        for (int j = 0; j < velikostPole; ++j)
        {
            if (_gameBoard[i][j] == 'W')
            {
                button = getButton(i,j);
                button->setStyleSheet("background-color: white");
            }
            if (_gameBoard[i][j] == 'B')
            {
                button = getButton(i,j);
                button->setStyleSheet("background-color: black");
            }

        }
    }
}

QLineEdit* obsah;

// save okno
void deska::on_pushButton_4_clicked()
{
    _save = new QWidget();
    //popSave->setWindowFlags(Qt::Popup);
    _save->setWindowTitle("ulozeni");

    QPushButton* save = new QPushButton;
    QPushButton* cancel = new QPushButton;
    QLabel* label = new QLabel("jméno");
    QLineEdit* txt = new QLineEdit;
    QGridLayout* layout = new QGridLayout;

    save->setText("Uložit");
    cancel->setText("Zrušit");

    connect(cancel, SIGNAL(clicked(bool)), _save, SLOT(close()));
    connect(save, SIGNAL(clicked(bool)), this, SLOT(on_pushButton_save_clicked()));

    layout->addWidget(label, 0, 0);
    layout->addWidget(txt, 0, 1);
    layout->addWidget(save, 1, 0);
    layout->addWidget(cancel, 1, 1);
    _save->setLayout(layout);

    //ziskam globalni pristup k obsahu lineEditu
    obsah = txt;

    _save->show();
}

//callback na save button
void deska::on_pushButton_save_clicked()
{
    ulozHistorii(obsah);
}

void deska::ulozHistorii(QLineEdit* txt)
{
    QString saveName;
    saveName = txt->text();

    QFile file("./saved/"+saveName+".save");
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        stream << mod << endl;
        stream << velikostPole << endl;

        for (unsigned int i = 0; i < _hra->_list.size(); ++i)
        {
            for (int j = 0; j < velikostPole; ++j)
            {
                for (int k = 0; k < velikostPole; ++k)
                    stream << _hra->_list[i][j][k];
                stream << "\n";
            }
        }
    }
    file.close();
    _save->close();
}

char** deska::makeArray(int size)
{
    char** arr = new char*[size];
    for (int i = 0; i < size; ++i)
        arr[i] = new char[size];
    return arr;
}

// vrati tlacitko podle mista v poli
QPushButton* deska::getButton(int radek, int sloupec)
{
    if (radek == 0 && sloupec == 0)
        return (ui->butt0_0);
    else if (radek == 0 && sloupec == 1)
        return ui->butt0_1;
    else if (radek == 0 && sloupec == 2)
        return ui->butt0_2;
    else if (radek == 0 && sloupec == 3)
        return ui->butt0_3;
    else if (radek == 0 && sloupec == 4)
        return ui->butt0_4;
    else if (radek == 0 && sloupec == 5)
        return ui->butt0_5;
    else if (radek == 0 && sloupec == 6)
        return ui->butt0_6;
    else if (radek == 0 && sloupec == 7)
        return ui->butt0_7;
    else if (radek == 1 && sloupec == 0)
        return ui->butt1_0;
    else if (radek == 1 && sloupec == 1)
        return ui->butt1_1;
    else if (radek == 1 && sloupec == 2)
        return ui->butt1_2;
    else if (radek == 1 && sloupec == 3)
        return ui->butt1_3;
    else if (radek == 1 && sloupec == 4)
        return ui->butt1_4;
    else if (radek == 1 && sloupec == 5)
        return ui->butt1_5;
    else if (radek == 1 && sloupec == 6)
        return ui->butt1_6;
    else if (radek == 1 && sloupec == 7)
        return ui->butt1_7;
    else if (radek == 2 && sloupec == 0)
        return ui->butt2_0;
    else if (radek == 2 && sloupec == 1)
        return ui->butt2_1;
    else if (radek == 2 && sloupec == 2)
        return ui->butt2_2;
    else if (radek == 2 && sloupec == 3)
        return ui->butt2_3;
    else if (radek == 2 && sloupec == 4)
        return ui->butt2_4;
    else if (radek == 2 && sloupec == 5)
        return ui->butt2_5;
    else if (radek == 2 && sloupec == 6)
        return ui->butt2_6;
    else if (radek == 2 && sloupec == 7)
        return ui->butt2_7;
    else if (radek == 3 && sloupec == 0)
        return (ui->butt3_0);
    else if (radek == 3 && sloupec == 1)
        return (ui->butt3_1);
    else if (radek == 3 && sloupec == 2)
        return (ui->butt3_2);
    else if (radek == 3 && sloupec == 3)
        return (ui->butt3_3);
    else if (radek == 3 && sloupec == 4)
        return (ui->butt3_4);
    else if (radek == 3 && sloupec == 5)
        return (ui->butt3_5);
    else if (radek == 3 && sloupec == 6)
        return (ui->butt3_6);
    else if (radek == 3 && sloupec == 7)
        return (ui->butt3_7);
    else if (radek == 4 && sloupec == 0)
        return (ui->butt4_0);
    else if (radek == 4 && sloupec == 1)
        return (ui->butt4_1);
    else if (radek == 4 && sloupec == 2)
        return (ui->butt4_2);
    else if (radek == 4 && sloupec == 3)
        return (ui->butt4_3);
    else if (radek == 4 && sloupec == 4)
        return (ui->butt4_4);
    else if (radek == 4 && sloupec == 5)
        return (ui->butt4_5);
    else if (radek == 4 && sloupec == 6)
        return (ui->butt4_6);
    else if (radek == 4 && sloupec == 7)
        return (ui->butt4_7);
    else if (radek == 5 && sloupec == 0)
        return (ui->butt5_0);
    else if (radek == 5 && sloupec == 1)
        return (ui->butt5_1);
    else if (radek == 5 && sloupec == 2)
        return (ui->butt5_2);
    else if (radek == 5 && sloupec == 3)
        return (ui->butt5_3);
    else if (radek == 5 && sloupec == 4)
        return (ui->butt5_4);
    else if (radek == 5 && sloupec == 5)
        return (ui->butt5_5);
    else if (radek == 5 && sloupec == 6)
        return (ui->butt5_6);
    else if (radek == 5 && sloupec == 7)
        return (ui->butt5_7);
    else if (radek == 6 && sloupec == 0)
        return (ui->butt6_0);
    else if (radek == 6 && sloupec == 1)
        return (ui->butt6_1);
    else if (radek == 6 && sloupec == 2)
        return (ui->butt6_2);
    else if (radek == 6 && sloupec == 3)
        return (ui->butt6_3);
    else if (radek == 6 && sloupec == 4)
        return (ui->butt6_4);
    else if (radek == 6 && sloupec == 5)
        return (ui->butt6_5);
    else if (radek == 6 && sloupec == 6)
        return (ui->butt6_6);
    else if (radek == 6 && sloupec == 7)
        return (ui->butt6_7);
    else if (radek == 7 && sloupec == 0)
        return (ui->butt7_0);
    else if (radek == 7 && sloupec == 1)
        return (ui->butt7_1);
    else if (radek == 7 && sloupec == 2)
        return (ui->butt7_2);
    else if (radek == 7 && sloupec == 3)
        return (ui->butt7_3);
    else if (radek == 7 && sloupec == 4)
        return (ui->butt7_4);
    else if (radek == 7 && sloupec == 5)
        return (ui->butt7_5);
    else if (radek == 7 && sloupec == 6)
        return (ui->butt7_6);
    else
        return (ui->butt7_7);
}

void deska::on_butt0_0_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(0, 0, button);

    //QPushButton* button = qobject_cast<QPushButton*>(sender());
    //button->setStyleSheet("background-color: black");
}

void deska::on_butt0_1_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(0, 1, button);
}

void deska::on_butt0_2_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(0, 2, button);
}

void deska::on_butt0_3_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(0, 3, button);
}
void deska::on_butt0_4_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(0, 4, button);
}
void deska::on_butt0_5_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(0, 5, button);
}
void deska::on_butt0_6_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(0, 6, button);
}
void deska::on_butt0_7_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(0, 7, button);
}
void deska::on_butt1_0_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(1, 0, button);
}
void deska::on_butt1_1_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(1, 1, button);
}
void deska::on_butt1_2_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(1, 2, button);
}
void deska::on_butt1_3_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(1, 3, button);
}
void deska::on_butt1_4_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(1, 4, button);
}
void deska::on_butt1_5_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(1, 5, button);
}
void deska::on_butt1_6_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(1, 6, button);
}
void deska::on_butt1_7_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(1, 7, button);
}
void deska::on_butt2_0_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(2, 0, button);
}
void deska::on_butt2_1_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(2, 1, button);
}
void deska::on_butt2_2_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(2, 2, button);
}
void deska::on_butt2_3_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(2, 3, button);
}

int deska::score(char c)
{
    int count = 0;
    for (int i = 0; i < velikostPole; ++i)
    {
        for (int j = 0; j < velikostPole; ++j)
        {
            if (_herniDeska[i][j] == c)
                count++;
        }
    }
    return count;
}

//pouze debug funkce
void deska::uloz(char** arr, QString saveName)
{

    QFile file("C:\\Users\\Karel\\Documents\\hra2016\\saved\\"+saveName+".save");
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        stream << mod << endl;
        stream << velikostPole << endl;

            for (int j = 0; j < 8; ++j)
            {
                for (int k = 0; k < 8; ++k)
                    stream << arr[j][k];
                stream << "\n";
            }
    }
    file.close();
    //_save->close();
}

void deska::reset()
{
    QPushButton* button = new QPushButton;

    for (int i = 0; i < velikostPole; ++i)
    {
        for (int j = 0; j < velikostPole; ++j)
        {
            button = getButton(i, j);
            button->setStyleSheet("background-color: #778f9d");
        }
    }
}

//PREW
void deska::on_pushButton_clicked()
{
    if (_hra->jumpBack() == 0)
    {
        _herniDeska = _hra->getGameBoard();
        this->reset();
        arrToButton(_herniDeska);
        hrac += 2;
        playGame();
    }
    else
        return;
}


// NEXT
void deska::on_pushButton_2_clicked()
{
    if (_hra->jumpForward() == 0)
    {
        _herniDeska = _hra->getGameBoard();
        this->reset();
        arrToButton(_herniDeska);
        hrac += 2;
        playGame();
    }
    else
        return;
}

void deska::saveSpace(int a, int b, QPushButton* button)
{
    if (!validMoves(a, b, _legalMoves))
        return;

    provedTah(a, b);
    arrToButton(_herniDeska);
    colorBack();

    if (hrac % 2 == 0)
        button->setStyleSheet("background-color: black");
    else
        button->setStyleSheet("background-color: white");

    if (this->gameEnd())
    {
        QMessageBox* msg = new QMessageBox();
        msg->setText("<h2><b><u>KONEC HRY</u></b></h>");
        if (B > W)
            msg->setInformativeText("Cerny hrac vyhral!");
        else
            msg->setInformativeText("Bily hrac vyhral!");
        msg->show();
    }

    playGame();
}

void deska::printScores()
{
    B = score('B');
    W = score('W');
    QString score = "<h2><b>"+QString::number(B)+"</b></h>";

    //cerny hrac
    ui->textBrowser->setText(score);
    ui->textBrowser->setAlignment(Qt::AlignCenter);
    //bily hrac
    score = "<h2><b>"+QString::number(W)+"</b></h>";
    ui->textBrowser_2->setText(score);
    ui->textBrowser_2->setAlignment(Qt::AlignCenter);
}

bool deska::gameEnd()
{
    this->_herniDeska = _hra->getGameBoard();
    for (int i = 0; i < velikostPole; ++i)
    {
        for (int j = 0; j < velikostPole; ++j)
        {
            if (_herniDeska[i][j] == '-')
                return false;
        }
    }
    return true;
}

void deska::provedTah(int radek, int sloupec)
{
    if (hrac % 2 == 0)
        _hra->getEnemyTables(radek, sloupec, 'W', NULL, false);
    else
        _hra->getEnemyTables(radek, sloupec, 'B', NULL, false);

    hrac++;
}

// zrusi signalizaci po odehrani
void deska::colorBack()
{
    QPushButton* button;
    for(auto x: (*_legalMoves))
    {
        button = getButton(x.first, x.second);
        button->setStyleSheet("background-color: #778f9d");
    }
}

void deska::on_butt2_4_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(2, 4, button);

}
void deska::on_butt2_5_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(2, 5, button);

}
void deska::on_butt2_6_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(2, 6, button);

}
void deska::on_butt2_7_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(2, 7, button);

}
void deska::on_butt3_0_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(3, 0, button);

}
void deska::on_butt3_1_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(3, 1, button);

}
void deska::on_butt3_2_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(3, 2, button);

}
void deska::on_butt3_3_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(3, 3, button);

}
void deska::on_butt3_4_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(3, 4, button);

}
void deska::on_butt3_5_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(3, 5, button);

}
void deska::on_butt3_6_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(3, 6, button);

}
void deska::on_butt3_7_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(3, 7, button);

}
void deska::on_butt4_0_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(4, 0, button);

}
void deska::on_butt4_1_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(4, 1, button);

}
void deska::on_butt4_2_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(4, 2, button);

}
void deska::on_butt4_3_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(4, 3, button);

}
void deska::on_butt4_4_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(4, 4, button);

}
void deska::on_butt4_5_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(4, 5, button);

}
void deska::on_butt4_6_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(4, 6, button);

}
void deska::on_butt4_7_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(4, 7, button);

}
void deska::on_butt5_0_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(5, 0, button);

}
void deska::on_butt5_1_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(5, 1, button);
}
void deska::on_butt5_2_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(5, 2, button);

}
void deska::on_butt5_3_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(5, 3, button);

}
void deska::on_butt5_4_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(5, 4, button);

}
void deska::on_butt5_5_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(5, 5, button);

}
void deska::on_butt5_6_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(5, 6, button);

}
void deska::on_butt5_7_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(5, 7, button);

}
void deska::on_butt6_0_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(6, 0, button);

}
void deska::on_butt6_1_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(6, 1, button);

}
void deska::on_butt6_2_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(6, 2, button);

}
void deska::on_butt6_3_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(6, 3, button);

}
void deska::on_butt6_4_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(6, 4, button);

}
void deska::on_butt6_5_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(6, 5, button);

}
void deska::on_butt6_6_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(6, 6, button);

}
void deska::on_butt6_7_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(6, 7, button);

}
void deska::on_butt7_0_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(7, 0, button);

}
void deska::on_butt7_1_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(7, 1, button);

}
void deska::on_butt7_2_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(7, 2, button);
}
void deska::on_butt7_3_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(7, 3, button);

}
void deska::on_butt7_4_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(7, 4, button);

}
void deska::on_butt7_5_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(7, 5, button);

}
void deska::on_butt7_6_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(7, 6, button);

}
void deska::on_butt7_7_clicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    saveSpace(7, 7, button);

}

