/**
 *  @file deska.h
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Definice tridy deska.
 *
 */

#ifndef DESKA_H
#define DESKA_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include "game.h"
#include "AI.h"

namespace Ui {
class deska;
}

/*! @brief deska: Trida reprezentuje aktivni hraci plochu v GUI verzi.
 * Obsahuje veskere existujici operace nad GUI hraci deskou.
 */  
class deska : public QWidget
{
    Q_OBJECT

public:
    explicit deska(QWidget *parent = 0);
    /*! Konstruktor. Zobrazi herni plochu a inicializuje instanci tridy Game.
     * @param hra Reprezentuje nactenou hru, pokud se nehraje od zacatku.
     */ 
    deska(Game* hra);
    ~deska();
    /*! Metoda ktera konvertuje index z pole na prislusne tlacitko na herni desce
     * @return Vraci ukazatel na tlacitko
     */
    QPushButton* getButton(int radek, int sloupec);
    /*! Metoda ktere vytvori nove pole pro napr. pro dalsi tah
     */
    char** makeArray(int size);
    /*! Metoda volana pouze jednou pri kazdem spusteni.
     * Nastavi zakladni barevne schema hraci desky
     */
    void initColor();
    /*! Metoda ktera prevede informace z celeho pole do GUI (tlacitek)
     * @param arr Reprezentuje 2D pole obsahujici informace o aktualnich pozicich kamenu
     */
    void arrToButton(char** arr);
    /*! Ridici metoda pro zahajeni tahu jednoho z hracu.
     * Vola se na konci kazdeho kola pro zahajeni dalsiho.
     */
    void playGame();
    /*! Metoda pro kontrolu validniho tahu.
     * @param a Souradnice radku
     * @param b Souradnice sloupce
     * @param moves Container obsahujici vsechny mozne tahy daneho hrace.
     * @return Vraci true/false hodnotu proveditelnosti pozadovaneho tahu.
     */
    bool validMoves(int a, int b, std::multimap<int, int>* moves);
    /*! Metoda uklidi grafickou signalizaci moznych tahu na hraci plose.
    */
    void colorBack();
    /*! Metoda rozlisi hrace a posle pozadavek na zabrani kamenu.
     * @param radek Souradnice radku kde chce hrac polozit svuj kamen.
     * @param sloupec Souradnice sloupce kde chce hrac polozit svuj kamen.
     */
    void provedTah(int radek, int sloupec);
    /*! Metoda ridi pozadavek na polozeni kamene na zadane tlacitko
     * @param button Tlacitko na ktere chce hrac polozit kamen
     */
    void saveSpace(int a, int b, QPushButton* button);
    /*! Metoda kontroluje, zda-li nastal konec hry
    */
    bool gameEnd();
    /*! Metoda zjistujici score pro daneho hrace
     * @param typHrace Informace o typu hrace (cerny/bily)
     * @return vraci pocet obsazenych kamenu danym hracem
     */
    int score(char typHrace);
    /*! Metoda ktera tiskne skore obou hracu na desku
     */
    void printScores();
    /*! Metoda ktera resi ulozeni rozehrane hry
    */
    void ulozHistorii(QLineEdit* );
    /*! Metoda tiskne na desku informaci o tom, ktery z hracu je prave na tahu
     */
    void printPlayer();
    /*! Debug metoda pouze pro testovaci ucely
     * Tiskne reprezentaci pole do souboru
     */
    void uloz(char** arr, QString saveName); //DEGUG
    /*! Metoda vraci stav hraci plochy po prechodu do predchoziho kroku
    */
    void reset();
    /*! Metoda vytvori nove 2D pole a inicializuje tim pole ve hre.
    */
    void deskaSetup();
    
    Game* _hra; ///< Instance tridy Game, ktera bude obsahovat informace o stavu hry

public slots:
    void on_pushButton_save_clicked();

private slots:
    void on_pushButton_5_clicked();

    void on_pushButton_4_clicked();

    void on_butt0_0_clicked();
    void on_butt0_1_clicked();
    void on_butt0_2_clicked();
    void on_butt0_3_clicked();
    void on_butt0_4_clicked();
    void on_butt0_5_clicked();
    void on_butt0_6_clicked();
    void on_butt0_7_clicked();
    void on_butt1_0_clicked();
    void on_butt1_1_clicked();
    void on_butt1_2_clicked();
    void on_butt1_3_clicked();
    void on_butt1_4_clicked();
    void on_butt1_5_clicked();
    void on_butt1_6_clicked();
    void on_butt1_7_clicked();
    void on_butt2_0_clicked();
    void on_butt2_1_clicked();
    void on_butt2_2_clicked();
    void on_butt2_3_clicked();
    void on_butt2_4_clicked();
    void on_butt2_5_clicked();
    void on_butt2_6_clicked();
    void on_butt2_7_clicked();
    void on_butt3_0_clicked();
    void on_butt3_1_clicked();
    void on_butt3_2_clicked();
    void on_butt3_3_clicked();
    void on_butt3_4_clicked();
    void on_butt3_5_clicked();
    void on_butt3_6_clicked();
    void on_butt3_7_clicked();
    void on_butt4_0_clicked();
    void on_butt4_1_clicked();
    void on_butt4_2_clicked();
    void on_butt4_3_clicked();
    void on_butt4_4_clicked();
    void on_butt4_5_clicked();
    void on_butt4_6_clicked();
    void on_butt4_7_clicked();
    void on_butt5_0_clicked();
    void on_butt5_1_clicked();
    void on_butt5_2_clicked();
    void on_butt5_3_clicked();
    void on_butt5_4_clicked();
    void on_butt5_5_clicked();
    void on_butt5_6_clicked();
    void on_butt5_7_clicked();
    void on_butt6_0_clicked();
    void on_butt6_1_clicked();
    void on_butt6_2_clicked();
    void on_butt6_3_clicked();
    void on_butt6_4_clicked();
    void on_butt6_5_clicked();
    void on_butt6_6_clicked();
    void on_butt6_7_clicked();
    void on_butt7_0_clicked();
    void on_butt7_1_clicked();
    void on_butt7_2_clicked();
    void on_butt7_3_clicked();
    void on_butt7_4_clicked();
    void on_butt7_5_clicked();
    void on_butt7_6_clicked();
    void on_butt7_7_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    QWidget* _save;  ///< okno ktere se objevi pri stisknuti tlacitka save
    Ui::deska *ui;  ///<  rozhrani celeho okna
    std::multimap<int, int>* _legalMoves;  ///< seznam vsech moznych tahu pro hrace ktery je zrovna na tahu
    char** _herniDeska;   ///< meziuloziste aktualniho stavu herni desky
    QPushButton* butt0_0;
    QPushButton* butt0_1;
    QPushButton* butt0_2;
    QPushButton* butt0_3;
    QPushButton* butt0_4;
    QPushButton* butt0_5;
    QPushButton* butt0_6;
    QPushButton* butt0_7;
    QPushButton* butt1_0;
    QPushButton* butt1_1;
    QPushButton* butt1_2;
    QPushButton* butt1_3;
    QPushButton* butt1_4;
    QPushButton* butt1_5;
    QPushButton* butt1_6;
    QPushButton* butt1_7;
    QPushButton* butt2_0;
    QPushButton* butt2_1;
    QPushButton* butt2_2;
    QPushButton* butt2_3;
    QPushButton* butt2_4;
    QPushButton* butt2_5;
    QPushButton* butt2_6;
    QPushButton* butt2_7;
    QPushButton* butt3_0;
    QPushButton* butt3_1;
    QPushButton* butt3_2;
    QPushButton* butt3_3;
    QPushButton* butt3_4;
    QPushButton* butt3_5;
    QPushButton* butt3_6;
    QPushButton* butt3_7;
    QPushButton* butt4_0;
    QPushButton* butt4_1;
    QPushButton* butt4_2;
    QPushButton* butt4_3;
    QPushButton* butt4_4;
    QPushButton* butt4_5;
    QPushButton* butt4_6;
    QPushButton* butt4_7;
    QPushButton* butt5_0;
    QPushButton* butt5_1;
    QPushButton* butt5_2;
    QPushButton* butt5_3;
    QPushButton* butt5_4;
    QPushButton* butt5_5;
    QPushButton* butt5_6;
    QPushButton* butt5_7;
    QPushButton* butt6_0;
    QPushButton* butt6_1;
    QPushButton* butt6_2;
    QPushButton* butt6_3;
    QPushButton* butt6_4;
    QPushButton* butt6_5;
    QPushButton* butt6_6;
    QPushButton* butt6_7;
    QPushButton* butt7_0;
    QPushButton* butt7_1;
    QPushButton* butt7_2;
    QPushButton* butt7_3;
    QPushButton* butt7_4;
    QPushButton* butt7_5;
    QPushButton* butt7_6;
    QPushButton* butt7_7;

};

#endif // DESKA_H
