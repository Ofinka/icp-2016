/**
 *  @file game.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Implementace metod tridy Game.
 *
 */

#include "game.h"
#include "tools.h"
#include "shared.h"
#include <fstream>
#include <dirent.h>
#include <string.h>

using namespace std;

extern int idHrace;
extern int errorIndicate;
bool prazdnySeznam = false;

Game::Game(char** board, int size)
{
    _gameBoard = board;
    _size = size;
    
    this->printInitBoard(_gameBoard);
    
    for (auto x : _list)
        delete x;
    
    
    // nacteni zakladni desky do seznamu historie
    char** arr = new char*[_size];
    for (int i = 0; i < _size; ++i)
        arr[i] = new char[_size];
    
    this->printInitBoard(arr);
    
    _index = -1;
    
    setHistory(arr);
}

void Game::printInitBoard(char** board)
{
    for (int i = 0; i < _size; ++i)
    {
        //cislo sloupce
        for (int j = 0; j < _size; ++j)
        {
            if (i == ((_size/2)-1) && j == ((_size/2)-1))
                board[i][j] = 'W';
            else if (i == ((_size/2)-1) && j == (_size/2))
                board[i][j] = 'B';
            else if (i == (_size/2) && j == ((_size/2)-1))
                board[i][j] = 'B';
            else if (i == (_size/2) && j == (_size/2))
                board[i][j] = 'W';
            else
                board[i][j] = '-';
        }
    }
}

// vycet vsech smeru ktere jsou prohledavany pro kazdy kamen
enum smery
{
    DOLU_NEBO_DOLEVA,
    NAHORU_NEBO_DOPRAVA,
    NAHORU_A_DOLEVA,
    DOLU_A_DOPRAVA,
    DOLU_A_DOLEVA,
    NAHORU_A_DOPRAVA
};

// zjisti vsechny validni souradnice pro tah daneho hrace
bool Game::getLegalMoves(int rada, int sloupec)
{
    std::multimap<int, int> *legalMoves = new std::multimap<int, int>();
    
    // hraje cerny hrac
    if (idHrace % 2 == 1)
    {
        this->checkDirections(legalMoves, 'B');
    }
    else
    {
        this->checkDirections(legalMoves, 'W');
    }
    
    if (legalMoves->begin() == legalMoves->end())
        prazdnySeznam = true;
    
    for(auto x: (*legalMoves))
        if (x.first != rada || x.second != sloupec)
            continue;
        else
            return true;
        
    return false;
}

// pro gui
std::multimap<int, int>* Game::getLegalMoves(int* hrac)
{
    std::multimap<int, int> *legalMoves = new std::multimap<int, int>();

    // hraje cerny hrac
    if ((*hrac) % 2 == 1)
    {
        this->checkDirections(legalMoves, 'B');
    }
    else
    {
        this->checkDirections(legalMoves, 'W');
    }
    return legalMoves;
}

// jestli je mozne polozit svuj kamen na zvolene pole
bool Game::canSet(int radek, int sloupec)
{
    // prekryvani
    if (_gameBoard[radek][sloupec] != '-')
        return false;
    return true;
}

// najde jiz polozene kameny ve vsech smerech
// zavola funkci findPair, ktera ulozi validni kombinace techto souradnic
void Game::checkDirections(multimap<int, int> *directions, char blackWhite)
{
    bool nasel = false;
    int tmpRadek;
    int tmpSloupec;
    char search = 'B';
    
    if (blackWhite == 'B')
        search = 'W';
    
    
    for (int radek = 0; radek < _size; ++radek)
    {
        for (int sloupec = 0; sloupec < _size; ++sloupec)
        {
            if (_gameBoard[radek][sloupec] != search)
                continue;
            
            // hledam dole vlevo, skutecna souradnice bude nahore vpravo
            tmpRadek = radek;
            tmpSloupec = sloupec;
            while(tmpRadek < _size-1 && tmpSloupec > 0)
            {
                tmpRadek++;
                tmpSloupec--;
                
                if (_gameBoard[tmpRadek][tmpSloupec] == '-')
                    break;

                if (_gameBoard[tmpRadek][tmpSloupec] != blackWhite)
                    continue;
                
                nasel = true;
            }
            if (nasel)
            {
                this->findPair(NAHORU_A_DOPRAVA, true, directions, radek, sloupec, blackWhite);
                nasel = false;
            }            
            // hledam nahore vpravo, skutecna bude dole vlevo
            tmpRadek = radek;
            tmpSloupec = sloupec;
            while(tmpRadek > 0 && tmpSloupec < _size-1)
            {
                tmpRadek--;
                tmpSloupec++;
                
                if (_gameBoard[tmpRadek][tmpSloupec] == '-')
                    break;

                if (_gameBoard[tmpRadek][tmpSloupec] != blackWhite)
                    continue;
                
                nasel = true;
            }
            if (nasel)
            {
                this->findPair(DOLU_A_DOLEVA, true, directions, radek, sloupec, blackWhite);
                nasel = false;
            }
            // nahoru doleva
            tmpRadek = radek;
            tmpSloupec = sloupec;
            while(tmpRadek < _size-1 && tmpSloupec < _size-1)
            {
                tmpRadek++;
                tmpSloupec++;
                
                if (_gameBoard[tmpRadek][tmpSloupec] == '-')
                    break;

                if (_gameBoard[tmpRadek][tmpSloupec] != blackWhite)
                    continue;
                
                nasel = true;
            }
            if (nasel)
            {
                this->findPair(NAHORU_A_DOLEVA, true, directions, radek, sloupec, blackWhite);
                nasel = false;
            }
            // dolu doprava
            tmpRadek = radek;
            tmpSloupec = sloupec;
            while(tmpRadek > 0 && tmpSloupec > 0)
            {
                tmpRadek--;
                tmpSloupec--;
                
                if (_gameBoard[tmpRadek][tmpSloupec] == '-')
                    break;

                if (_gameBoard[tmpRadek][tmpSloupec] != blackWhite)
                    continue;
                
                nasel = true;
            }
            if (nasel)
            {
                this->findPair(DOLU_A_DOPRAVA, true, directions, radek, sloupec, blackWhite);
                nasel = false;
            }
            tmpRadek = radek;
            // zeshora dolu
            while(tmpRadek < _size-1)
            {
                tmpRadek++;
                
                if (_gameBoard[tmpRadek][sloupec] == '-')
                    break;

                if (_gameBoard[tmpRadek][sloupec] != blackWhite)
                    continue;
                nasel = true;
            }
            if (nasel)
            {
                this->findPair(DOLU_NEBO_DOLEVA, true, directions, radek, sloupec, blackWhite);
                nasel = false;
            }
            // zespoda nahoru
            tmpRadek = radek;
            while(tmpRadek > 0)
            {
                tmpRadek--;

                if (_gameBoard[tmpRadek][sloupec] == '-')
                    break;

                if (_gameBoard[tmpRadek][sloupec] != blackWhite)
                    continue;
                
                nasel = true;
            }
            if (nasel)
            {
                this->findPair(NAHORU_NEBO_DOPRAVA, true, directions, radek, sloupec, blackWhite);
                nasel = false;
            }
            // zleva doprava
            tmpSloupec = sloupec;
            while(tmpSloupec > 0)
            {
                tmpSloupec--;

                if (_gameBoard[radek][tmpSloupec] == '-')
                    break;

                if (_gameBoard[radek][tmpSloupec] != blackWhite)
                    continue;
                nasel = true;
            }
            if (nasel)
            {
                this->findPair(NAHORU_NEBO_DOPRAVA, false, directions, radek, sloupec, blackWhite);
                nasel = false;
            }
            // zprava doleva
            tmpSloupec = sloupec;
            while(tmpSloupec < _size-1)
            {
                tmpSloupec++;

                if (_gameBoard[radek][tmpSloupec] == '-')
                    break;

                if (_gameBoard[radek][tmpSloupec] != blackWhite)
                    continue;
                nasel = true;
            }
            if (nasel)
            {
                this->findPair(DOLU_NEBO_DOLEVA, false, directions, radek, sloupec, blackWhite);
                nasel = false;
            }
        }
    }
}

// najde odpovidajici protejsek k jiz polozenym kamenum a ulozi vsechny validni souradnice do directions
void Game::findPair(int a, bool rowOrCol, multimap<int, int> *directions, int radek, int sloupec, char blackWhite)
{
    int var;
    bool nothing = true;
    
    if (rowOrCol)
        var = radek;
    else
        var = sloupec;
        
    switch (a)
    {
        case DOLU_NEBO_DOLEVA:
            while (var > 0)
            {
                var--;
                
                if(rowOrCol)
                {
                    if (_gameBoard[var][sloupec] == blackWhite)
                        break;
                    if (_gameBoard[var][sloupec] != '-')
                        continue;
                    else
                        nothing = false;
                }
                else
                {
                    if (_gameBoard[radek][var] == blackWhite)
                        break;
                    if (_gameBoard[radek][var] != '-')
                        continue;
                    else
                        nothing = false;
                }
                break;
            }
            break;
        case NAHORU_NEBO_DOPRAVA:
            while (var < _size-1)
            {
                var++;
                
                if(rowOrCol)
                {
                    if (_gameBoard[var][sloupec] == blackWhite)
                        break;
                    if (_gameBoard[var][sloupec] != '-')
                        continue;
                    else
                        nothing = false;
                }
                else
                {   
                    if (_gameBoard[radek][var] == blackWhite)
                        break;
                    if (_gameBoard[radek][var] != '-')
                        continue;
                    else
                        nothing = false;
                }
                break;
            }
            break;
        case DOLU_A_DOPRAVA:
            while (radek < _size-1 && sloupec < _size-1)
            {
                radek++;
                sloupec++;
                
                if (_gameBoard[radek][sloupec] == blackWhite)
                    break;
                if (_gameBoard[radek][sloupec] != '-')
                    continue;
                else
                    nothing = false;
                break;
            }
            break;
        case NAHORU_A_DOLEVA:
            while (radek > 0 && sloupec > 0)
            {
                radek--;
                sloupec--;
                
                if (_gameBoard[radek][sloupec] == blackWhite)
                    break;
                if (_gameBoard[radek][sloupec] != '-')
                    continue;
                else
                    nothing = false;
                break;
            }
            break;
        case NAHORU_A_DOPRAVA:
            while (radek > 0 && sloupec < _size-1)
            {
                radek--;
                sloupec++;
                
                if (_gameBoard[radek][sloupec] == blackWhite)
                    break;
                if (_gameBoard[radek][sloupec] != '-')
                    continue;
                else
                    nothing = false;
                break;
            }
            break;
        case DOLU_A_DOLEVA:
            while (radek < _size-1 && sloupec > 0)
            {
                radek++;
                sloupec--;
                
                if (_gameBoard[radek][sloupec] == blackWhite)
                    break;
                if (_gameBoard[radek][sloupec] != '-')
                    continue;
                else
                    nothing = false;
                break;
            }
            break;
    }

    // kdyz nenajdeme zadnou souradnici tak skip
    if (nothing)
        return;
    
    if (a == NAHORU_NEBO_DOPRAVA || a == DOLU_NEBO_DOLEVA)
    {
        // naplnim seznam dalsi souradnici
        if (rowOrCol)
            directions->insert(make_pair(var, sloupec));
        else
            directions->insert(make_pair(radek, var));
    }
    else
    {
        directions->insert(make_pair(radek, sloupec));
    }
}

// ukradne souperovi kameny podle pravidle hry
void Game::getEnemyTables(int rada, int sloupec, char signature, int* testCount, bool test)
{
    // vytvoreni duplicitniho pole 
    char** arr = new char*[_size];
    for (int i = 0; i < _size; ++i)
        arr[i] = new char[_size];
    
    // inicializuju pomocne pole aktualnimi hodnotami
    for (int i = 0; i < _size; ++i)
    {
        for (int j = 0; j < _size; ++j)
        arr[i][j] = _gameBoard[i][j];
    }
    
    arr[rada][sloupec] = 'N';
    bool check = false;

    // od bodu dolu
    for (int i = rada; i < _size; ++i)
    {
        if (arr[i][sloupec] == '-')
            break;
        else if (arr[i][sloupec] == signature && !check)
            continue;
        else if (arr[i][sloupec] == signature && check)
            break;
        else
        {
            for (int j = i+1; j < _size; ++j)
            {
                check = true;
                if (arr[j][sloupec] == '-')
                    break;
                if (arr[j][sloupec] == signature)
                {
                    arr[i][sloupec] = 'N';
                    break;
                }
            }
        }
    }check = false;
    // od bodu nahoru
    for (int i = rada; i > -1; --i)
    {
        if (arr[i][sloupec] == '-')
            break;
        else if (arr[i][sloupec] == signature && !check)
            continue;
        else if (arr[i][sloupec] == signature && check)
            break;
        else
        {
            for (int j = i-1; j > -1; --j)
            {
                check = true;
                if (arr[j][sloupec] == '-')
                    break;
                if (arr[j][sloupec] == signature)
                {
                    arr[i][sloupec] = 'N';
                    break;
                }
            }
        }
    }check = false;
    // od bodu doleva
    for (int i = sloupec; i > -1; --i)
    {
        if (arr[rada][i] == '-')
            break;
        else if (arr[rada][i] == signature && !check)
            continue;
        else if (arr[rada][i] == signature && check)
            break;
        else
        {
            for (int j = i-1; j > -1; --j)
            {
                check = true;
                if (arr[rada][j] == '-')
                    break;
                if (arr[rada][j] == signature)
                {
                    arr[rada][i] = 'N';
                    break;
                }
            }
        }
    }check = false;
    // od bodu doprava
    for (int i = sloupec; i < _size; ++i)
    {
        if (arr[rada][i] == '-')
            break;
        else if (arr[rada][i] == signature && !check)
            continue;
        else if (arr[rada][i] == signature && check)
            break;
        else
        {
            for (int j = i+1; j < _size; ++j)
            {
                check = true;
                if (arr[rada][j] == '-')
                    break;
                if (arr[rada][j] == signature)
                {
                    arr[rada][i] = 'N';
                    break;
                }
            }
        }
    }check = false;
    // od bodu sikmo dolu doprava
    for (int i = sloupec, r = rada; i > -1 && r > -1; --i, --r)
    {
        if (arr[r][i] == '-')
            break;
        else if (arr[r][i] == signature && !check)
            continue;
        else if (arr[r][i] == signature && check)
            break;
        else
        {
            for (int j = i-1, k = r-1; j > -1 && k > -1; --j, --k)
            {
                check = true;
                if (arr[k][j] == '-')
                    break;
                if (arr[k][j] == signature)
                {
                    arr[r][i] = 'N';
                    break;
                }
            }
        }
    }check = false;
    // od bodu sikmo nahoru doleva
    for (int i = sloupec, r = rada; i < _size && r < _size; ++i, ++r)
    {
        if (arr[r][i] == '-')
            break;
        else if (arr[r][i] == signature && !check)
            continue;
        else if (arr[r][i] == signature && check)
            break;
        else
        {
            for (int j = i+1, k = r+1; j < _size && k < _size; ++j, ++k)
            {
                check = true;
                if (arr[k][j] == '-')
                    break;
                if (arr[k][j] == signature)
                {
                    arr[r][i] = 'N';
                    break;
                }
            }
        }
    }check = false;
    // od bodu sikmo dolu doleva
    for (int i = sloupec, r = rada; i < _size && r > -1; ++i, --r)
    {
        if (arr[r][i] == '-')
            break;
        else if (arr[r][i] == signature && !check)
            continue;
        else if (arr[r][i] == signature && check)
            break;
        else
        {
            for (int j = i+1, k = r-1; j < _size && k > -1; ++j, --k)
            {
                check = true;
                if (arr[k][j] == '-')
                    break;
                if (arr[k][j] == signature)
                {
                    arr[r][i] = 'N';
                    break;
                }
            }
        }
    }check = false;
    // od bodu sikmo nahoru doprava
    for (int i = sloupec, r = rada; i > -1 && r < _size; --i, ++r)
    {
        if (arr[r][i] == '-')
            break;
        else if (arr[r][i] == signature && !check)
            continue;
        else if (arr[r][i] == signature && check)
            break;
        else
        {
            for (int j = i-1, k = r+1; j > -1 && k < _size; --j, ++k)
            {
                check = true;
                if (arr[k][j] == '-')
                    break;
                if (arr[k][j] == signature)
                {
                    arr[r][i] = 'N';
                    break;
                }
            }
        }
    }
    // tento usek plati pouze pro single player na porovnani nejvhodnejsiho kroku
    if (test == true)
    {
        for (int i = 0; i < _size; ++i)
        {
            for (int j = 0; j < _size; ++j)
            {
                if (arr[i][j] == 'N')
                    arr[i][j] = signature;  
                if (arr[i][j] == 'W')
                    (*testCount)++;
            }
        }
        
        return;
    } 
    
    // prepisu mista ktera se zmenila
    for (int i = 0; i < _size; ++i)
    {
        for (int j = 0; j < _size; ++j)
        {
            if (arr[i][j] == 'N')
                arr[i][j] = signature;  
            _gameBoard[i][j] = arr[i][j];
        }
    }
    
    // kvuli rezii historie tahu musim provest jeste jeden check
    this->setHistory(arr);
}

void Game::setGameBoard(char** arr, int size)
{
    for (int i = 0; i < size; ++i)
    {
        for (int j = 0; j < size; ++j)
        {
            _gameBoard[i][j] = arr[i][j];
        }
    }
}


// stara se o stav historie
void Game::setHistory(char** arr)
{
    //printList();

    // kdyz dojde k prepsani historie musim nahradit kroky ktere jiz neplati
    for (unsigned int i = _index+1; i < _list.size();)
        _list.pop_back();

    _list.push_back(arr);
    _index++;
}

// pomocna funkce pro vytisk historie tahu
void Game::printList()
{
    for (unsigned int k = 0; k < _list.size(); ++k)
    {
        for (int i = 0; i < _size; ++i)
        {
            //cislo sloupce
            for (int j = 0; j < _size; ++j)
            {
                cout << _list[k][i][j];
            }
            cout << endl;
        }
    }
}

// zkontroluje validitu pozadavku a polozi kamen na zadanou souradnici
void Game::setValue(int &rada, int &sloupec)
{
    // nemuzu polozit kamen na jiz polozeny
    if (!this->canSet(rada, sloupec))
    {
        errorIndicate = CANT_SET_VALUE;
        return;
    }
    
    // kontrola zda je mozne polozit kamen na stanovene misto podle pravidel hry v zakladnich smerech
    if (!this->getLegalMoves(rada, sloupec))
    {    
        errorIndicate = CANT_SET_VALUE_2;
        return;
    }
    
    if (idHrace % 2 == 0)
        this->getEnemyTables(rada, sloupec, 'W', NULL, false);
    else
        this->getEnemyTables(rada, sloupec, 'B', NULL, false);
    
    idHrace++;
}

int Game::jumpBack()
{
    // kdyz se chci vratit pred hraci desku na ktere se zacinalo doslo by k segfaultu
    if ((_index -2) < 0)
        return 1;
    _index -= 2; 
    
    // aktualni herni desku musim predat po hodnotach protoze bych si jinak prepsal historii
    for (int i = 0; i < _size; ++i)
    {
        //cislo sloupce
        for (int j = 0; j < _size; ++j)
        {
            _gameBoard[i][j] = _list[_index][i][j];
        }
    }
    
    idHrace += 2;
    return 0;
}

int Game::jumpForward()
{
    // kdyz chci skocit za posledni evidovany tah doslo by k segfaultu
    unsigned tmp = _index + 2;
    if (tmp >= _list.size())
        return 1;
    _index += 2;
    
    // aktualni herni desku musim predat po hodnotach protoze bych si jinak prepsal historii
    for (int i = 0; i < _size; ++i)
    {
        //cislo sloupce
        for (int j = 0; j < _size; ++j)
        {
            _gameBoard[i][j] = _list[_index][i][j];
        }
    }
    
    idHrace += 2;
    return 0;
}

void Game::saveGame()
{
    // zjistim od hrace jak chce hru pojmenovat aby ji pozdeji mohl snaze najit
    string fileName;

jmenoSouboru:
    cout << "Zvolte jmeno pro ulozeni hry:";
    cin >> fileName;
    fileName = "saved/" + fileName + ".save";
    
    if (ifstream(fileName))
    {
        char c;
        cout << "Takto pojmenovana hra jiz existuje, prepsat? [y/n]:" << endl;
        cin >> c;
        if (c == 'n' || c != 'y')
            goto jmenoSouboru;
    }
    
    // pripravim si soubor
    ofstream file;
    file.open(fileName);
    
    file << _gameMode << endl;
    file << _size << endl;
    // zapisu do souboru celou historii her 
    // aby bylo mozne po nacteni hry pouzivat prew a next
    for (unsigned int i = 0; i < _list.size(); ++i)
    {
        for (int j = 0; j < _size; ++j)
        {
            for (int k = 0; k < _size; ++k)
                file << _list[i][j][k];
            file << "\n";
        }
    }
    
    // zavru soubor a pokracuju ve hre
    file.close();
}

void Game::getDataFromFile(const std::string &name)
{
    ifstream readFile;
    readFile.open(name);
    
    // kontrola jestli se soubor otevrel
    if (!readFile.is_open())
    {
        errorIndicate = OPEN_FILE;
        return;
    }
    
    // prectu si prvni polozku coz znaci herni mod (multi/single)
    while (!readFile.eof())
    {
        readFile >> this->_gameMode;
        break;
    }
    
    char n1;
    bool cifra = true;
    // prectu si druhou polozku ktera znaci velikost herni desky
    while (!readFile.eof())
    {
        readFile >> n1;

        if (!cifra)
        {
            if (n1 == '0')
                this->_size = 10;
            else
                this->_size = 12;
            break;
        }
        if (n1 == '8')
        {
            this->_size = 8;
            break;
        }
        else
            cifra = false;
    }
    
    // musim smazat zakladni desku ktera se nahrala pred touto cinnosti
    _list.pop_back();
        
    char c;
    int cisloRadku = 0;
    int cisloSloupce = 0;
    this->_index = -1;

    // vytvorim polozku vectoru na pushback
    char** arr = new char*[_size];
    for (int i = 0; i < _size; ++i)
        arr[i] = new char[_size];
    
    while (!readFile.eof())
    {
        readFile >> c;

        // kdyz nactu vic vzorku nez je velikost hraci desky musim prejit na dalsi radek
        if (cisloSloupce == _size)
        {
            cisloRadku++;
            cisloSloupce = 0;
        }
        //kdyz naplnim jeden prvek tak ho musim pushnout a naalokovat novy
        if (cisloRadku == _size)
        {
            setHistory(arr);
            arr = makeArray(_size);
            cisloRadku = 0;
            cisloSloupce = 0;
        }
        arr[cisloRadku][cisloSloupce] = c;
        cisloSloupce++;
    }
    
    // na konec nastavim posledni tah jako aktualni a muzu pokracovat ve hre
    for (int i = 0; i < _size; ++i)
    {
        for (int j = 0; j < _size; ++j)
        _gameBoard[i][j] = _list[_index][i][j]; 
    }

    if ((_list.size() %2) == 0)
        idHrace = 0;
    else
        idHrace = 1;
}

char** Game::makeArray(int size)
{
    char** arr = new char*[size];
    for (int i = 0; i < size; ++i)
        arr[i] = new char[size];
    return arr;
}

void Game::loadGame()
{
    string fileName;
    
    /* nabidnu uzivateli aby si zvolil nekterou nahranych her */
    int skip = 0;
    int count = 1;
    DIR* dpdf;
    struct dirent *epdf;
    
    dpdf = opendir("./saved/");
    if (dpdf != NULL)
    {
        cout << " == Seznam ulozenych her ==\n";
        while (epdf = readdir(dpdf))
        {
            // nechci zobrazovat linuxove odkazy na soucasny a rodicovsky adresar (.) (..)
            //skip++;
            if ((strcmp(epdf->d_name, ".") == 0) || (strcmp(epdf->d_name, "..") == 0)) 
                continue;
                
            // vytisknu vsechny ulozene hry
            // tato funkce predpoklada ze se ve slozce nachazeji pouze programem vytvorene soubory
            cout << " | " << count << ".  " << epdf->d_name << endl;
            count++;
        }
        cout << " == Zvolte nazev ulozene hry: ";
        cin >> fileName;
    }
    else
    {
        errorIndicate = NO_GAMES;
        return;
    }
    
    // nactu ulozene info ze souboru
    fileName = "./saved/" + fileName;
    this->getDataFromFile(fileName);
}
