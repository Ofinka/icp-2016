/**
 *  @file game.h
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Definice tridy Game.
 *
 */

#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdbool.h>
#include <vector>
#include <map>
#include "tools.h"

/*! @brief Game: Trida reprezentuje vsechny informace o provedenych tazich.
 * Poskytuje rozhrani nad hernimy moznostmi a obsahuje vsechny informace o plose.
 */
class Game : public Tools
{
    public:
        char _gameMode;             ///< single/multi - hlavne pro nahrani ulozene hry
        /*! Konstruktor tridy Game.
         * inicializuje vsechny atributy a vytiskne zakladni plochu pro CLI
         * @param arr 2D reprezentace herni desky
         * @param size Informace o velikosti herni plochy
         */
        Game(char** arr, int size);
        
        // prace se souradnicemi
        /*! Metoda na kontrolu pozadovaneho tahu daneho hrace
         * @param rada Souradnice radku kam chce hrac polozit kamen
         * @param sloupec Souradnice sloupce kam chce hrac polozit kamen
         * @return Vraci true/false proveditelnosti pozadovaneho tahu
         */
        bool getLegalMoves(int rada, int sloupec);
        /*! Metoda zjisti vsechny mozne tahy daneho hrace
         * @return Vraci seznam moznych tahu
         */
        std::multimap<int, int>* getLegalMoves(int*);
        /*! Metoda kontroluje jestli je mozne polozit kamen na dane pole
         * @param arr Aktualni hraci deska
         */
        bool canSet(char** arr, int radek, int sloupec);
        /*! Metoda najde jiz polozene kameny ve vsech smerech
         * dale vola metodu findPair
         */
        void checkDirections(std::multimap<int, int> *directions, char blackWhite);
        /*! Metoda najde odpovidajici protejsek k jiz polozenym kamenum
         * @param directions Seznam ktery naplnuje povolenymi souradnicemi
         * @param blackWhite Informace pro ktereho hrace se hleda
         */
        void findPair(int a, bool rowOrCol, std::multimap<int, int> *directions, int radek, int sloupec, char blackWhite);

        // pokladani kamenu
        /*! Metoda ktera prepise kameny jednoho hrace na kameny druheho hrace
         * @param signature Ci kameny se budou prepisovat
         */
        void getEnemyTables(int rada, int sloupec, char signature, int*, bool);
        /*! Metoda zkontroluje validitu pozadavku a polozi kamen na zadanou souradnici
         */
        void setValue(int &rada, int &sloupec);
        /*! Metoda kontrolujicijestli je mozne polozit svuj kamen na zvolene pole
         * @return Vraci true/false proveditelnosti polozeni kamene
         */
        bool canSet(int radek, int sloupec);
        
        // prace s historii a ukladanim 
        /*! Debug metoda
         */
        void initList();
        /*! Metoda ktera se stara o stav historie pri prepsani
        */
        void setHistory(char**); // pomocna pro next/prew
        /*! Metoda ktera ridi operaci prechodu na predesly tah
         * @return Vraci informaci o chybe ktera mohla nastat pri pokusu o provedeni skoku
         */
        int jumpBack();         //prew
        /*! Metoda ktera ridi operaci prechodu na nasledujici tah
         * @return Vraci informaci o chybe ktera mohla nastat pri pokusu o provedeni skoku
         */
        int jumpForward();      //next
        /*! Metoda ktera se stara o ukladani hry pri CLI verzi
         */
        void saveGame();
        /*! Metoda ktera se stara o nacteni ulozene hry pri CLI verzi
         */
        void loadGame();
        /*! Metoda pouzivana pri nacitani hry, pro ziskani informaci z ulozeneho .save souboru
         * @param name Nazev ulozene hry
         */
        void getDataFromFile(const std::string &name);
        
        // ostatni
        /*! Metoda ktera vytvori nove 2D pole
         * @param size Specifikace velikosti nove vytvoreneho pole
         * @return vraci ukazatel na vytvorene pole
         */
        char** makeArray(int size);
        /*! Pristup k privatni polozce _size
         */
        int returnSize() { return _size; }
        /*! Metoda tiskne aktualni stav hry na CLI
        */
        void printGame() { Tools::printGame(_gameBoard, _size); }
        /*! Metoda vytiskne historii tahu do souboru
         */
        void printList();
        /*! Metoda nastavi zakladni hraci plochu
         * @param arr Pole ktere bude nastaveno na defaultni hodnoty hraci desky
         */
        void printInitBoard(char** arr);
        /*! Pristup k privatni polozce _gameBoard
         */
        char** getGameBoard() { return this->_gameBoard; }
        /*! Nastaveni privatni polozky _gameBoard
         */
        void setGameBoard(char** arr, int size);
        void setSize(int n) { _size = n; }

        int _index;                 ///< aktualni index v historii
        std::vector<char**> _list;  ///< historie tahu

    private:
        //int _index;                 // aktualni index v historii
        //std::vector<char**> _list;  // historie tahu
        int _size;                  ///< velikost hraciho pole
        char** _gameBoard;          ///< aktualni stav hraci desky
};

#endif
