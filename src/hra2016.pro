#-------------------------------------------------
#
# Project created by QtCreator 2016-05-03T15:18:32
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11 qt
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hra2016
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    loadgame.cpp\
    run_cli.cpp\
    AI.cpp\
    tools.cpp\
    game.cpp \
    novahra.cpp \
    deska.cpp

HEADERS  += mainwindow.h \
    loadgame.h\
    AI.h\
    tools.h\
    shared.h\
    game.h \
    cli.h \
    novahra.h \
    deska.h

FORMS    += mainwindow.ui \
    loadgame.ui \
    novahra.ui \
    deska.ui
