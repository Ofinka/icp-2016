/**
 *  @file loadgame.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Implementacni tridy loadgame
 *
 */

#include "loadgame.h"
#include "ui_loadgame.h"
#include <QString>
#include "mainwindow.h"
#include <qdir.h>
#include "deska.h"
#include <fstream>
#include <iostream>
#include <dirent.h>


extern int velikostPole;
extern char mod;
extern int hrac;
extern int nahrana;

loadGame::loadGame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::loadGame)
{
    ui->setupUi(this);
    this->setWindowTitle("Nahrat hru");
    QDir recoredDir("./saved/");
    QStringList allFiles = recoredDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
    ui->listWidget->addItems(allFiles);
}

loadGame::~loadGame()
{
    delete ui;
}

char** loadGame::makeArray(int size)
{
    char** arr = new char*[size];
    for (int i = 0; i < size; ++i)
        arr[i] = new char[size];
    return arr;
}


void loadGame::on_pushButton_clicked()
{
    QString name = ui->listWidget->currentItem()->text();

    //udelam si objekt do kteryho nasledne nahraju rozehranou hru
    char** arr = makeArray(12);
    Game* gejm = new Game(arr, 12);


    //ulozHistorii(gejm, "wtf1");

    getData(name, gejm);
    nahrana = 1;

    //ulozHistorii(gejm, "wtf");

    deska* loadedGame = new deska(gejm);
    //loadedGame->_hra = gejm;
    loadedGame->show();
    this->hide();
}
/*
void loadGame::ulozHistorii(Game* _hra, QString saveName)
{
    QFile file("C:\\Users\\Karel\\Documents\\hra2016\\saved\\"+saveName+".save");
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        stream << mod << endl;
        stream << velikostPole << endl;

        for (int i = 0; i < _hra->_list.size(); ++i)
        {
            for (int j = 0; j < velikostPole; ++j)
            {
                for (int k = 0; k < velikostPole; ++k)
                    stream << _hra->_list[i][j][k];
                stream << "\n";
            }
        }
    }
    file.close();
}*/

void loadGame::getData(const QString name, Game* hra)
{
    using namespace std;
    ifstream readFile;
    string tmp = "./saved/"+name.toStdString();
    readFile.open(tmp.c_str());

    // kontrola jestli se soubor otevrel
    if (!readFile.is_open())
        return;

    // prectu si prvni polozku coz znaci herni mod (multi/single)
    while (!readFile.eof())
    {
        readFile >> mod;
        break;
    }

    char n1;
    bool cifra = true;
    // prectu si druhou polozku ktera znaci velikost herni desky
    while (!readFile.eof())
    {
        readFile >> n1;

        if (!cifra)
        {
            if (n1 == '0')
                velikostPole = 10;
            else
                velikostPole = 12;
            break;
        }
        if (n1 == '8')
        {
            velikostPole = 8;
            break;
        }
        else
            cifra = false;
    }

    // musim smazat zakladni desku ktera se nahrala pred touto cinnosti
    hra->_list.pop_back();

    char c;
    int cisloRadku = 0;
    int cisloSloupce = 0;
    hra->_index = -1;
    hra->setSize(velikostPole);


    // vytvorim polozku vectoru na pushback
    char** arr = new char*[velikostPole];
    for (int i = 0; i < velikostPole; ++i)
        arr[i] = new char[velikostPole];

    while (!readFile.eof())
    {
        readFile >> c;

        // kdyz nactu vic vzorku nez je velikost hraci desky musim prejit na dalsi radek
        if (cisloSloupce == velikostPole)
        {
            cisloRadku++;
            cisloSloupce = 0;
        }
        //kdyz naplnim jeden prvek tak ho musim pushnout a naalokovat novy
        if (cisloRadku == velikostPole)
        {
            hra->setHistory(arr);
            arr = makeArray(velikostPole);
            cisloRadku = 0;
            cisloSloupce = 0;
        }
        arr[cisloRadku][cisloSloupce] = c;
        cisloSloupce++;
    }

    tmpArr = hra->getGameBoard();
    // na konec nastavim posledni tah jako aktualni a muzu pokracovat ve hre
    for (int i = 0; i < velikostPole; ++i)
    {
        for (int j = 0; j < velikostPole; ++j)
            tmpArr[i][j] = hra->_list[hra->_index][i][j];
    }
    hra->setGameBoard(tmpArr, velikostPole);

    if ((hra->_list.size() %2) == 0)
        hrac = 0;
    else
        hrac = 1;
}

void loadGame::on_pushButton_2_clicked()
{
    MainWindow* ret = new MainWindow;
    ret->show();
    this->hide();
}
