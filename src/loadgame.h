/**
 *  @file loadgame.h
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Header tridy loadGame, ktera reprezentuje pouze gui okno pri nacitani existujici hry.
 *
 */

#ifndef LOADGAME_H
#define LOADGAME_H

#include <QWidget>
#include <QtCore>
#include <QtGui>
#include "game.h"
#include <QLineEdit>

namespace Ui {
class loadGame;
}

class loadGame : public QWidget
{
    Q_OBJECT

public:
    explicit loadGame(QWidget *parent = 0);
    ~loadGame();

    /*! Metoda priradi obsah save souboru do objektu Game
     * @param name Jmeno ulozene hry
     * @hra Instance tridy Game, ktera bude predana hlavni funkci programu
     */
    void getData(const QString name, Game* hra);
    char** makeArray(int size);
    //void ulozHistorii(Game* _hra, QString txt);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    char** tmpArr;
    Ui::loadGame *ui;
};

#endif // LOADGAME_H
