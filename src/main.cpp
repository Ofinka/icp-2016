/**
 *  @file main.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Main funkce pro gui
 *
 */

#include "mainwindow.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
