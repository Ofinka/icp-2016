/**
 *  @file main_cli.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Main funkce pro CLI. Predevsim na ziskani informaci o velikosti a stavu hry
 *
 */

#include "cli.h"

extern int idHrace;
extern int errorIndicate;
extern bool prazdnySeznam;

using namespace std;

int main(int argc, char** argv)
{
    int sloupce = 12;
    char mode, uroven;
    bool multiplayer;
    string option;

menu:    
    cout << "      == MENU ==\n";
    cout << "1) Nacist hru     [load]\n";
    cout << "2) Hrat novou hru [play]\n";
    cout << "3) Ukoncit        [exit]\n";
    cin >> option;
    
    if (option == "load")
        goto load;
    if (option == "exit")
        exit(1);
    if (option != "play")
        goto menu;
    
velikostDesky:
    cout << "Zadejte velikost hraci desky[6,8,10,12]" << endl;
    cin >> sloupce;
    
    if (sloupce != 6 && sloupce != 8 && sloupce != 10 && sloupce != 12)
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        handleErr(BOARD_SIZE);
        goto velikostDesky;
    }
 
herniMod: 
    cout << "Zvolte single player [S] nebo multiplayer [M]" << endl;
    cin >> mode;
    
    if (mode != 'S' && mode != 'M')
    {    
        handleErr(GAME_MODE);
        goto herniMod;
    }
    
    if (mode == 'S')
    {
obtiznost: 
        cout << "Zvolte obtiznost easy [E] nebo medium [M]" << endl;
        cin >> uroven;
        
        if (uroven != 'E' && uroven != 'M')
        {    
            handleErr(GAME_MODE);
            goto obtiznost;
        }
    }

load:    
    char** arr = new char*[sloupce];
    for (int i = 0; i < sloupce; ++i)
        arr[i] = new char[sloupce];
    
    Game* hra = new Game(arr, sloupce);
    
    if (mode == 'M')
        multiplayer = true;
    else
        multiplayer = false;
    
    hra->_gameMode = mode;
    
    if (option == "load")
        hra->loadGame();
    
    playGame(hra, uroven);
}


















