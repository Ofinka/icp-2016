/**
 *  @file mainwindow.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 *  Vygenerovano Qt pro hlavni menu gui.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

int velikostPole = 8;
char mod = 'E';
int nahrana = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Menu");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    load.show();
    this->hide();
}

void MainWindow::on_pushButton_3_clicked()
{
    newGame.show();
    this->hide();
}
