/**
 *  @file mainwindow.h
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Header tridy mainwindow, ktera reprezentuje zakladni gui menu.
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "loadgame.h"
#include "novahra.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    loadGame load;
    novaHra newGame;
};

#endif // MAINWINDOW_H
