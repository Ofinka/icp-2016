/**
 *  @file novahra.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Implementacni tridy novaHra
 *
 */
 
#include "novahra.h"
#include "ui_novahra.h"
#include "mainwindow.h"
#include "deska.h"

extern int velikostPole;
extern char mod;

novaHra::novaHra(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::novaHra)
{
    ui->setupUi(this);
    this->setWindowTitle("Nova hra");
}

novaHra::~novaHra()
{
    delete ui;
}

void novaHra::on_pushButton_2_clicked()
{
    MainWindow* ret = new MainWindow;
    ret->show();
    this->hide();
}

void novaHra::on_pushButton_clicked()
{
    deska* hra = new deska;
    hra->show();
    this->hide();
}

// lehka obtiznost
void novaHra::on_radioButton_clicked()
{
    mod = 'E';
}

// stredni obtiznost
void novaHra::on_radioButton_2_clicked()
{
    mod = 'S';
}

// multiplayer
void novaHra::on_radioButton_3_clicked()
{
    mod = 'M';
}

void novaHra::on_radioButton_4_clicked()
{
    velikostPole = 8;
}

void novaHra::on_radioButton_5_clicked()
{
    velikostPole = 10;
}

void novaHra::on_radioButton_6_clicked()
{
    velikostPole = 12;
}

void novaHra::on_radioButton_7_clicked()
{
    velikostPole = 6;
}
