/**
 *  @file novahra.h
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Header tridy novahra, ktera reprezentuje gui okno pri voleni velikosti a obtiznosti nove hry.
 *
 */

#ifndef NOVAHRA_H
#define NOVAHRA_H

#include <QWidget>

namespace Ui {
class novaHra;
}

class novaHra : public QWidget
{
    Q_OBJECT

public:
    explicit novaHra(QWidget *parent = 0);
    ~novaHra();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_radioButton_clicked();

    void on_radioButton_2_clicked();

    void on_radioButton_3_clicked();

    void on_radioButton_4_clicked();

    void on_radioButton_5_clicked();

    void on_radioButton_6_clicked();

    void on_radioButton_7_clicked();

private:
    Ui::novaHra *ui;
};

#endif // NOVAHRA_H
