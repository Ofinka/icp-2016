/**
 *  @file run_cli.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Soubor obsahuje implementace funkci z cli.h
 *
 */

#include "cli.h"

using namespace std;

int idHrace = 1;
int errorIndicate = 0;
extern bool prazdnySeznam;

void handleErr(int error)
{
    switch (error)
    {
        case BOARD_SIZE:
            fprintf(stderr, "\n!!! Velikost herni desky musi byt 8,10 nebo 12 !!!\n");
            break;
        case GAME_MODE:
            fprintf(stderr, "\n!!! Musite vybrat mezi single player [S] a multiplayer [M] mody !!!\n");
            break;
        case BAD_MOVE:
            fprintf(stderr, "\n!!! Tah musi byt ve tvaru [RADEK][SLOUPEC] !!!\n");
            break;
        case BAD_CHAR:
            fprintf(stderr, "\n!!! Zadana souradnice nebo prikaz neexistuje, zvolte prosim platne souradnice !!!\n");
            break;
        case CANT_SET_VALUE:
            fprintf(stderr, "\n!!! Neni mozno obsadit jiz obsazene pole, zvolte platne pole !!!\n");
            break;
        case CANT_SET_VALUE_2:
            fprintf(stderr, "\n!!! Zadane pole nelze obsadit, zvolte jine !!!\n");
            break;
        case CANT_SKIP:
            fprintf(stderr, "\n!!! Nemuzete vynechat kolo kdyz mate moznost tahnout !!!\n");
            break;
        case CANT_JUMP_F:
            fprintf(stderr, "\n!!! Nemuzete skocit o dalsi tah dopredu !!!\n");
            break;
        case CANT_JUMP_P:
            fprintf(stderr, "\n!!! Nemuzete se vratit o dalsi tah !!!\n");
            break;
        case NO_GAMES:
            fprintf(stderr, "\n!!! Nebyla nalezena zadna ulozena hra !!!\n");
            break;
        case OPEN_FILE:
            fprintf(stderr, "\n!!! Chyba pri otevirani souboru !!!\n");
            break;
        default:
            break;
    }
    errorIndicate = 0;
}

// rozlisuje prikazy zadane hracem v prubehu hry
int playTimeCommands(const string &tah)
{
    if (tah == "skip")
    {
        if (prazdnySeznam)
        {
            idHrace++;
            return 1;
        }
        errorIndicate = CANT_SKIP;
        return SKIP;
    }
    else if (tah == "exit")
        exit(0);
    else if (tah == "prew")
        return PREW;
    else if (tah == "next")
        return NEXT;
    else if (tah == "save")
        return SAVE;
    else if (tah == "load")
        return LOAD;
    else if (tah == "menu")
    {
        cout << "poresim pozdeji\n";
        return MENU;
    }
    else if (tah == "return")
    {
        cout << "poresim pozdeji\n";
        return RETURN;
    }
    else
        return 0;
}

void playGame(Game* hra, char uroven)
{
    int a, b;
    int *rada = &a; 
    int *sloupec = &b;
    int historie = 0;    
    
    while(1)
    {
        hra->printGame();
        
        if (hra->_gameMode == 'M')
        {
            if (idHrace % 2 == 0)
                cout << "\n\t=== Hrac W je na tahu ===\n" << "vynechat [skip] | konec hry [exit] | ulozit [save \"name\"]\n" << "  menu [return] |    zpet [prew]   | dopredu [next]\n";
            else
                cout << "\n\t=== Hrac B je na tahu ===\n" << "vynechat [skip] | konec hry [exit] | ulozit [save \"name\"]\n" << "  menu [return] |    zpet [prew]   | dopredu [next]\n";
        }
        else
        {
            if (idHrace % 2 == 1)
                cout << "\n\t=== Hrac B je na tahu ===\n" << "vynechat [skip] | konec hry [exit] | ulozit [save \"name\"]\n" << "  menu [return] |    zpet [prew]   | dopredu [next]\n";
            else
            {
                if (uroven == 'E')  // easy random choice strategy
                    randomStrategyEnemy(hra);
                else                // medium strategy
                    greatestProfitAlg(hra);
                continue;
            }
                
        }
        
        if (errorIndicate != 0)
            handleErr(errorIndicate);
        
        historie = hra->zadaneSouradnice(hra->returnSize(), sloupec, rada);
        
        // ziskani a kontrola souradnic
        if (historie == PREW)
        {
            if (hra->jumpBack() == 1)
            {
                errorIndicate = CANT_JUMP_P;
                continue;
            }
        }
        if (historie == NEXT)
        {
            if (hra->jumpForward() == 1)
            {
                errorIndicate = CANT_JUMP_F;
                continue;
            }
        }
        if (historie == SAVE)
        {
            hra->saveGame();
            continue;
        }
        if (historie == LOAD)
        {
            hra->loadGame();
            continue;
        }
        if (historie != 0)
            continue;
        
        
        // nastavim hodnotu a vytisknu aktualni board
        hra->setValue(*rada, *sloupec);
    }
}
