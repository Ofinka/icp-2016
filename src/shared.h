/**
 * @file shared.h
 * @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 *
 * 
 */

#ifndef SHARED_H
#define SHARED_H

/**
 * Seznam chyb ktere mohou nastat pri cli verzi programu
 */
enum errCodes
{
    BOARD_SIZE          = 10,
    GAME_MODE,
    BAD_MOVE,
    BAD_CHAR,
    CANT_SET_VALUE,
    CANT_SET_VALUE_2,
    CANT_SKIP,
    CANT_JUMP_P,
    CANT_JUMP_F,
    NO_GAMES,
    OPEN_FILE
};

/*! Funkce rozpoznava chyby ktere nastaly
 */
void handleErr(int error);


    
#endif
