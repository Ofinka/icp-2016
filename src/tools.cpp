/**
 *  @file tools.cpp
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Implementace tridy Tools
 *
 */

#include "tools.h"
#include "shared.h"

using namespace std;

int Tools::charToIndex(char c, int limit)
{
    char arr[12] = {'a','b','c','d','e','f','g','h','i','j','k','l'};
    
    for (int i = 0; i < 12; ++i)
    {
        if (c == arr[i] && i < limit)
            return i;
    }
    handleErr(BAD_CHAR);
    return 99;
}

bool Tools::isNumber(const string& s)
{
    string::const_iterator it = s.begin();
    
    while (it != s.end() && isdigit(*it)) 
        ++it;
    
    return !s.empty() && it == s.end();
}

bool Tools::isAlpha(const string& str)
{
    for (string::const_iterator it = str.begin(); it != str.end(); ++it)
    {
        if (isalpha(*it))
            continue;
        else
            return false;
    }
    return true;
}

int Tools::zadaneSouradnice(int size, int *sloupec, int *rada)
{
    string tah;
    int reakce = 0;
    cin >> tah;
        
    // reseni vsech prikazu ktere muze hrac zadat
    reakce = playTimeCommands(tah);
    if (reakce != 0)
        return reakce;
    
    // zadny prikaz nebyl zadan -> jedna se o chybu nebo souradnici
    // v pripade ze je tah ve tvaru [3b,8b,9c,..]
    if (isNumber(tah.substr(0,1)) && isAlpha(tah.substr(1,2)))
    {
        // ulozim si zadane souradnice
        *rada = stoi(tah.substr(0,1)) -1;
        *sloupec = charToIndex(tah[1], size);
    }
    // v pripade ze je tah ve tvaru [10b, 11h,..]
    else if (isNumber(tah.substr(0,2)) && isAlpha(tah.substr(2,3)))
    {
        // ulozim si zadane souradnice
        *rada = stoi(tah.substr(0,2)) -1;
        *sloupec = charToIndex(tah[2], size);
    }
    else
    {
        errorIndicate = BAD_CHAR;
        return 1;
    }
    
    // spatne zadana souradnice -> hrac dostane moznost zadat ji znovu a spravne
    if (*sloupec == 99)
    {
        errorIndicate = BAD_CHAR;
        return 1;
    }
    if (*rada < 0 || *rada >= size)
    {
        errorIndicate = BAD_CHAR;
        return 1;
    }
    return 0;
}

void Tools::printAlphabet(int width)
{
    char alphabet[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l' };

    cout << " ";
    for (int i = 0; i < width; ++i)
        cout << "  " << alphabet[i];
    cout << endl;
}

int Tools::printScore(char** pole, int size)
{
    int scoreBlack = 0;
    int scoreWhite = 0;
    bool konecHry = true;
    
    for (int radek = 0; radek < size; ++radek)
    {
        for (int sloupec = 0; sloupec < size; ++sloupec)
        {
            if (pole[radek][sloupec] == '-')
            {
                konecHry = false;
                continue;
            }
            else if (pole[radek][sloupec] == 'B')
                scoreBlack++;
            else if (pole[radek][sloupec] == 'W')
                scoreWhite++;
        }
    }
    cout << "\t    B[" << scoreBlack << "]:W[" << scoreWhite << "]\n";
    if (konecHry)
    {
        if (scoreBlack < scoreWhite)
            cout << "\tHRAC W VYHRAL!!!\n";
        else
            cout << "\tHRAC B VYHRAL!!!\n";
        return 1;
    }
    if (scoreBlack == 0)
    {
        cout << "\tHRAC W VYHRAL!!!\n";
        return 1;
    }
    if (scoreWhite == 0)
    {
        cout << "\tHRAC B VYHRAL!!!\n";
        return 1;
    }
    return 0;
}

void Tools::printGame(char** pole, int sloupce)
{
    int konec = 0;
    // vytisknu ramec souradnici pro uzivatele
    cout << endl << "\t       STAV\n";
    konec = printScore(pole, sloupce);
    cout << endl;
    printAlphabet(sloupce);
    
    for (int i = 1; i <= sloupce; ++i)
    {
        //kdyz je dvojmistne cislo tak musim ubrat mezeru aby nebyla vizualni kolize
        if (i < 10)
            cout << i << "  ";
        else
            cout << i << " ";
        for (int j = 0; j < sloupce; ++j)
        {
            cout << pole[i-1][j] << "  ";

        }
        cout << i << endl;
    }
    
    // vytisknu ho i dole kvuli designu
    printAlphabet(sloupce);
    if (konec == 1)
        exit(0);
}