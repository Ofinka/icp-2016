/**
 *  @file tools.h
 *  @author Karel Pokorny, xpokor68@stud.fit.vutbr.cz
 * 
 *  Header tridy Tools.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdbool.h>
#include <string> //stoi

#ifndef TOOLS_H
#define TOOLS_H

extern int errorIndicate;
extern int playTimeCommands(const std::string &tah);

/*! @brief Tools: Trida ktera obsahuje zakladni nastroje pro zobrazeni stavu hry v CLI verzi programu
 * Prevadi a kontroluje vstupy a vystupy CLI programu.
 */
class Tools
{
    public:
        /*! Metoda prevadi zadanou souradnici sloupce z pismene na index
         * @param c Pismeno reprezentujici zadanou souradnici
         * @param limit Limit vymezujici velikost herni plochy
         * @return Vraci cislo indexu
         */
        int charToIndex(char c, int limit);
        /*! Metoda kontroluje jestli je zadany string cislo
         * @param s Vstupni retezec
         */
        bool isNumber(const std::string& s);
        /*! Metoda kontroluje jestli je zadany string slozeny pouze z pismen
         * @param s Vstupni retezec
         */
        bool isAlpha(const std::string& str);

        /*! Metoda ktera kontroluje validitu zadanych souradnic pro CLI
         */
        int zadaneSouradnice(int, int*, int*);

        /*! Metoda ktera vytiskne na CLI sloupcove souradnice
         * @param width Parametr sirky reprezentuje kolik souradnic se ma vytisknout
         */
        void printAlphabet(int width);
        /*! Metoda tiskne skore obou hracu pro CLI
         */
        int printScore(char**, int);
        /*! Metoda tiskne aktualni stav hraci desky na CLI
         */
        void printGame(char**, int);
};

#endif