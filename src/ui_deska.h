/********************************************************************************
** Form generated from reading UI file 'deska.ui'
**
** Created: Thu May 5 23:37:17 2016
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DESKA_H
#define UI_DESKA_H

#include <QtCore/QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QTextBrowser>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_deska
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *butt0_0;
    QPushButton *butt0_1;
    QPushButton *butt0_2;
    QPushButton *butt0_3;
    QPushButton *butt0_4;
    QPushButton *butt0_5;
    QPushButton *butt0_6;
    QPushButton *butt0_7;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *butt1_0;
    QPushButton *butt1_1;
    QPushButton *butt1_2;
    QPushButton *butt1_3;
    QPushButton *butt1_4;
    QPushButton *butt1_5;
    QPushButton *butt1_6;
    QPushButton *butt1_7;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *butt2_0;
    QPushButton *butt2_1;
    QPushButton *butt2_2;
    QPushButton *butt2_3;
    QPushButton *butt2_4;
    QPushButton *butt2_5;
    QPushButton *butt2_6;
    QPushButton *butt2_7;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *butt3_0;
    QPushButton *butt3_1;
    QPushButton *butt3_2;
    QPushButton *butt3_3;
    QPushButton *butt3_4;
    QPushButton *butt3_5;
    QPushButton *butt3_6;
    QPushButton *butt3_7;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *butt4_0;
    QPushButton *butt4_1;
    QPushButton *butt4_2;
    QPushButton *butt4_3;
    QPushButton *butt4_4;
    QPushButton *butt4_5;
    QPushButton *butt4_6;
    QPushButton *butt4_7;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *butt5_0;
    QPushButton *butt5_1;
    QPushButton *butt5_2;
    QPushButton *butt5_3;
    QPushButton *butt5_4;
    QPushButton *butt5_5;
    QPushButton *butt5_6;
    QPushButton *butt5_7;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *butt6_0;
    QPushButton *butt6_1;
    QPushButton *butt6_2;
    QPushButton *butt6_3;
    QPushButton *butt6_4;
    QPushButton *butt6_5;
    QPushButton *butt6_6;
    QPushButton *butt6_7;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *butt7_0;
    QPushButton *butt7_1;
    QPushButton *butt7_2;
    QPushButton *butt7_3;
    QPushButton *butt7_4;
    QPushButton *butt7_5;
    QPushButton *butt7_6;
    QPushButton *butt7_7;
    QLabel *label;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_11;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_4;
    QTextBrowser *textBrowser;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_5;
    QTextBrowser *textBrowser_2;

    void setupUi(QWidget *deska)
    {
        if (deska->objectName().isEmpty())
            deska->setObjectName(QString::fromUtf8("deska"));
        deska->resize(600, 400);
        layoutWidget = new QWidget(deska);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(70, 310, 241, 58));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_2 = new QPushButton(layoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        pushButton_3 = new QPushButton(layoutWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout->addWidget(pushButton_3);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pushButton_4 = new QPushButton(layoutWidget);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        horizontalLayout_2->addWidget(pushButton_4);

        pushButton_5 = new QPushButton(layoutWidget);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        horizontalLayout_2->addWidget(pushButton_5);

        pushButton_6 = new QPushButton(layoutWidget);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        horizontalLayout_2->addWidget(pushButton_6);


        verticalLayout->addLayout(horizontalLayout_2);

        layoutWidget1 = new QWidget(deska);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(40, 40, 308, 244));
        verticalLayout_2 = new QVBoxLayout(layoutWidget1);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        butt0_0 = new QPushButton(layoutWidget1);
        butt0_0->setObjectName(QString::fromUtf8("butt0_0"));
        butt0_0->setIconSize(QSize(16, 16));

        horizontalLayout_3->addWidget(butt0_0);

        butt0_1 = new QPushButton(layoutWidget1);
        butt0_1->setObjectName(QString::fromUtf8("butt0_1"));
        butt0_1->setIconSize(QSize(16, 16));

        horizontalLayout_3->addWidget(butt0_1);

        butt0_2 = new QPushButton(layoutWidget1);
        butt0_2->setObjectName(QString::fromUtf8("butt0_2"));
        butt0_2->setIconSize(QSize(16, 16));

        horizontalLayout_3->addWidget(butt0_2);

        butt0_3 = new QPushButton(layoutWidget1);
        butt0_3->setObjectName(QString::fromUtf8("butt0_3"));
        butt0_3->setIconSize(QSize(16, 16));

        horizontalLayout_3->addWidget(butt0_3);

        butt0_4 = new QPushButton(layoutWidget1);
        butt0_4->setObjectName(QString::fromUtf8("butt0_4"));
        butt0_4->setIconSize(QSize(16, 16));

        horizontalLayout_3->addWidget(butt0_4);

        butt0_5 = new QPushButton(layoutWidget1);
        butt0_5->setObjectName(QString::fromUtf8("butt0_5"));
        butt0_5->setIconSize(QSize(16, 16));

        horizontalLayout_3->addWidget(butt0_5);

        butt0_6 = new QPushButton(layoutWidget1);
        butt0_6->setObjectName(QString::fromUtf8("butt0_6"));
        butt0_6->setIconSize(QSize(16, 16));

        horizontalLayout_3->addWidget(butt0_6);

        butt0_7 = new QPushButton(layoutWidget1);
        butt0_7->setObjectName(QString::fromUtf8("butt0_7"));
        butt0_7->setIconSize(QSize(16, 16));

        horizontalLayout_3->addWidget(butt0_7);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        butt1_0 = new QPushButton(layoutWidget1);
        butt1_0->setObjectName(QString::fromUtf8("butt1_0"));
        butt1_0->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(butt1_0);

        butt1_1 = new QPushButton(layoutWidget1);
        butt1_1->setObjectName(QString::fromUtf8("butt1_1"));
        butt1_1->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(butt1_1);

        butt1_2 = new QPushButton(layoutWidget1);
        butt1_2->setObjectName(QString::fromUtf8("butt1_2"));
        butt1_2->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(butt1_2);

        butt1_3 = new QPushButton(layoutWidget1);
        butt1_3->setObjectName(QString::fromUtf8("butt1_3"));
        butt1_3->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(butt1_3);

        butt1_4 = new QPushButton(layoutWidget1);
        butt1_4->setObjectName(QString::fromUtf8("butt1_4"));
        butt1_4->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(butt1_4);

        butt1_5 = new QPushButton(layoutWidget1);
        butt1_5->setObjectName(QString::fromUtf8("butt1_5"));
        butt1_5->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(butt1_5);

        butt1_6 = new QPushButton(layoutWidget1);
        butt1_6->setObjectName(QString::fromUtf8("butt1_6"));
        butt1_6->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(butt1_6);

        butt1_7 = new QPushButton(layoutWidget1);
        butt1_7->setObjectName(QString::fromUtf8("butt1_7"));
        butt1_7->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(butt1_7);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        butt2_0 = new QPushButton(layoutWidget1);
        butt2_0->setObjectName(QString::fromUtf8("butt2_0"));
        butt2_0->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(butt2_0);

        butt2_1 = new QPushButton(layoutWidget1);
        butt2_1->setObjectName(QString::fromUtf8("butt2_1"));
        butt2_1->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(butt2_1);

        butt2_2 = new QPushButton(layoutWidget1);
        butt2_2->setObjectName(QString::fromUtf8("butt2_2"));
        butt2_2->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(butt2_2);

        butt2_3 = new QPushButton(layoutWidget1);
        butt2_3->setObjectName(QString::fromUtf8("butt2_3"));
        butt2_3->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(butt2_3);

        butt2_4 = new QPushButton(layoutWidget1);
        butt2_4->setObjectName(QString::fromUtf8("butt2_4"));
        butt2_4->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(butt2_4);

        butt2_5 = new QPushButton(layoutWidget1);
        butt2_5->setObjectName(QString::fromUtf8("butt2_5"));
        butt2_5->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(butt2_5);

        butt2_6 = new QPushButton(layoutWidget1);
        butt2_6->setObjectName(QString::fromUtf8("butt2_6"));
        butt2_6->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(butt2_6);

        butt2_7 = new QPushButton(layoutWidget1);
        butt2_7->setObjectName(QString::fromUtf8("butt2_7"));
        butt2_7->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(butt2_7);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        butt3_0 = new QPushButton(layoutWidget1);
        butt3_0->setObjectName(QString::fromUtf8("butt3_0"));
        butt3_0->setIconSize(QSize(16, 16));

        horizontalLayout_6->addWidget(butt3_0);

        butt3_1 = new QPushButton(layoutWidget1);
        butt3_1->setObjectName(QString::fromUtf8("butt3_1"));
        butt3_1->setIconSize(QSize(16, 16));

        horizontalLayout_6->addWidget(butt3_1);

        butt3_2 = new QPushButton(layoutWidget1);
        butt3_2->setObjectName(QString::fromUtf8("butt3_2"));
        butt3_2->setIconSize(QSize(16, 16));

        horizontalLayout_6->addWidget(butt3_2);

        butt3_3 = new QPushButton(layoutWidget1);
        butt3_3->setObjectName(QString::fromUtf8("butt3_3"));
        butt3_3->setAutoFillBackground(false);
        butt3_3->setIconSize(QSize(16, 16));

        horizontalLayout_6->addWidget(butt3_3);

        butt3_4 = new QPushButton(layoutWidget1);
        butt3_4->setObjectName(QString::fromUtf8("butt3_4"));
        butt3_4->setIconSize(QSize(16, 16));

        horizontalLayout_6->addWidget(butt3_4);

        butt3_5 = new QPushButton(layoutWidget1);
        butt3_5->setObjectName(QString::fromUtf8("butt3_5"));
        butt3_5->setIconSize(QSize(16, 16));

        horizontalLayout_6->addWidget(butt3_5);

        butt3_6 = new QPushButton(layoutWidget1);
        butt3_6->setObjectName(QString::fromUtf8("butt3_6"));
        butt3_6->setIconSize(QSize(16, 16));

        horizontalLayout_6->addWidget(butt3_6);

        butt3_7 = new QPushButton(layoutWidget1);
        butt3_7->setObjectName(QString::fromUtf8("butt3_7"));
        butt3_7->setIconSize(QSize(16, 16));

        horizontalLayout_6->addWidget(butt3_7);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        butt4_0 = new QPushButton(layoutWidget1);
        butt4_0->setObjectName(QString::fromUtf8("butt4_0"));
        butt4_0->setIconSize(QSize(16, 16));

        horizontalLayout_7->addWidget(butt4_0);

        butt4_1 = new QPushButton(layoutWidget1);
        butt4_1->setObjectName(QString::fromUtf8("butt4_1"));
        butt4_1->setIconSize(QSize(16, 16));

        horizontalLayout_7->addWidget(butt4_1);

        butt4_2 = new QPushButton(layoutWidget1);
        butt4_2->setObjectName(QString::fromUtf8("butt4_2"));
        butt4_2->setIconSize(QSize(16, 16));

        horizontalLayout_7->addWidget(butt4_2);

        butt4_3 = new QPushButton(layoutWidget1);
        butt4_3->setObjectName(QString::fromUtf8("butt4_3"));
        butt4_3->setIconSize(QSize(16, 16));

        horizontalLayout_7->addWidget(butt4_3);

        butt4_4 = new QPushButton(layoutWidget1);
        butt4_4->setObjectName(QString::fromUtf8("butt4_4"));
        butt4_4->setIconSize(QSize(16, 16));

        horizontalLayout_7->addWidget(butt4_4);

        butt4_5 = new QPushButton(layoutWidget1);
        butt4_5->setObjectName(QString::fromUtf8("butt4_5"));
        butt4_5->setIconSize(QSize(16, 16));

        horizontalLayout_7->addWidget(butt4_5);

        butt4_6 = new QPushButton(layoutWidget1);
        butt4_6->setObjectName(QString::fromUtf8("butt4_6"));
        butt4_6->setIconSize(QSize(16, 16));

        horizontalLayout_7->addWidget(butt4_6);

        butt4_7 = new QPushButton(layoutWidget1);
        butt4_7->setObjectName(QString::fromUtf8("butt4_7"));
        butt4_7->setIconSize(QSize(16, 16));

        horizontalLayout_7->addWidget(butt4_7);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        butt5_0 = new QPushButton(layoutWidget1);
        butt5_0->setObjectName(QString::fromUtf8("butt5_0"));
        butt5_0->setIconSize(QSize(16, 16));

        horizontalLayout_8->addWidget(butt5_0);

        butt5_1 = new QPushButton(layoutWidget1);
        butt5_1->setObjectName(QString::fromUtf8("butt5_1"));
        butt5_1->setIconSize(QSize(16, 16));

        horizontalLayout_8->addWidget(butt5_1);

        butt5_2 = new QPushButton(layoutWidget1);
        butt5_2->setObjectName(QString::fromUtf8("butt5_2"));
        butt5_2->setIconSize(QSize(16, 16));

        horizontalLayout_8->addWidget(butt5_2);

        butt5_3 = new QPushButton(layoutWidget1);
        butt5_3->setObjectName(QString::fromUtf8("butt5_3"));
        butt5_3->setIconSize(QSize(16, 16));

        horizontalLayout_8->addWidget(butt5_3);

        butt5_4 = new QPushButton(layoutWidget1);
        butt5_4->setObjectName(QString::fromUtf8("butt5_4"));
        butt5_4->setIconSize(QSize(16, 16));

        horizontalLayout_8->addWidget(butt5_4);

        butt5_5 = new QPushButton(layoutWidget1);
        butt5_5->setObjectName(QString::fromUtf8("butt5_5"));
        butt5_5->setIconSize(QSize(16, 16));

        horizontalLayout_8->addWidget(butt5_5);

        butt5_6 = new QPushButton(layoutWidget1);
        butt5_6->setObjectName(QString::fromUtf8("butt5_6"));
        butt5_6->setIconSize(QSize(16, 16));

        horizontalLayout_8->addWidget(butt5_6);

        butt5_7 = new QPushButton(layoutWidget1);
        butt5_7->setObjectName(QString::fromUtf8("butt5_7"));
        butt5_7->setIconSize(QSize(16, 16));

        horizontalLayout_8->addWidget(butt5_7);


        verticalLayout_2->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        butt6_0 = new QPushButton(layoutWidget1);
        butt6_0->setObjectName(QString::fromUtf8("butt6_0"));
        butt6_0->setIconSize(QSize(16, 16));

        horizontalLayout_9->addWidget(butt6_0);

        butt6_1 = new QPushButton(layoutWidget1);
        butt6_1->setObjectName(QString::fromUtf8("butt6_1"));
        butt6_1->setIconSize(QSize(16, 16));

        horizontalLayout_9->addWidget(butt6_1);

        butt6_2 = new QPushButton(layoutWidget1);
        butt6_2->setObjectName(QString::fromUtf8("butt6_2"));
        butt6_2->setIconSize(QSize(16, 16));

        horizontalLayout_9->addWidget(butt6_2);

        butt6_3 = new QPushButton(layoutWidget1);
        butt6_3->setObjectName(QString::fromUtf8("butt6_3"));
        butt6_3->setIconSize(QSize(16, 16));

        horizontalLayout_9->addWidget(butt6_3);

        butt6_4 = new QPushButton(layoutWidget1);
        butt6_4->setObjectName(QString::fromUtf8("butt6_4"));
        butt6_4->setIconSize(QSize(16, 16));

        horizontalLayout_9->addWidget(butt6_4);

        butt6_5 = new QPushButton(layoutWidget1);
        butt6_5->setObjectName(QString::fromUtf8("butt6_5"));
        butt6_5->setIconSize(QSize(16, 16));

        horizontalLayout_9->addWidget(butt6_5);

        butt6_6 = new QPushButton(layoutWidget1);
        butt6_6->setObjectName(QString::fromUtf8("butt6_6"));
        butt6_6->setIconSize(QSize(16, 16));

        horizontalLayout_9->addWidget(butt6_6);

        butt6_7 = new QPushButton(layoutWidget1);
        butt6_7->setObjectName(QString::fromUtf8("butt6_7"));
        butt6_7->setIconSize(QSize(16, 16));

        horizontalLayout_9->addWidget(butt6_7);


        verticalLayout_2->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        butt7_0 = new QPushButton(layoutWidget1);
        butt7_0->setObjectName(QString::fromUtf8("butt7_0"));
        butt7_0->setIconSize(QSize(16, 16));

        horizontalLayout_10->addWidget(butt7_0);

        butt7_1 = new QPushButton(layoutWidget1);
        butt7_1->setObjectName(QString::fromUtf8("butt7_1"));
        butt7_1->setIconSize(QSize(16, 16));

        horizontalLayout_10->addWidget(butt7_1);

        butt7_2 = new QPushButton(layoutWidget1);
        butt7_2->setObjectName(QString::fromUtf8("butt7_2"));
        butt7_2->setIconSize(QSize(16, 16));

        horizontalLayout_10->addWidget(butt7_2);

        butt7_3 = new QPushButton(layoutWidget1);
        butt7_3->setObjectName(QString::fromUtf8("butt7_3"));
        butt7_3->setIconSize(QSize(16, 16));

        horizontalLayout_10->addWidget(butt7_3);

        butt7_4 = new QPushButton(layoutWidget1);
        butt7_4->setObjectName(QString::fromUtf8("butt7_4"));
        butt7_4->setEnabled(true);
        butt7_4->setIconSize(QSize(16, 16));

        horizontalLayout_10->addWidget(butt7_4);

        butt7_5 = new QPushButton(layoutWidget1);
        butt7_5->setObjectName(QString::fromUtf8("butt7_5"));
        butt7_5->setIconSize(QSize(16, 16));

        horizontalLayout_10->addWidget(butt7_5);

        butt7_6 = new QPushButton(layoutWidget1);
        butt7_6->setObjectName(QString::fromUtf8("butt7_6"));
        butt7_6->setIconSize(QSize(16, 16));

        horizontalLayout_10->addWidget(butt7_6);

        butt7_7 = new QPushButton(layoutWidget1);
        butt7_7->setObjectName(QString::fromUtf8("butt7_7"));
        butt7_7->setIconSize(QSize(16, 16));

        horizontalLayout_10->addWidget(butt7_7);


        verticalLayout_2->addLayout(horizontalLayout_10);

        label = new QLabel(deska);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(380, 140, 191, 41));
        widget = new QWidget(deska);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(410, 40, 121, 61));
        horizontalLayout_11 = new QHBoxLayout(widget);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_4 = new QLabel(widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_3->addWidget(label_4);

        textBrowser = new QTextBrowser(widget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));

        verticalLayout_3->addWidget(textBrowser);


        horizontalLayout_11->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_5 = new QLabel(widget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_4->addWidget(label_5);

        textBrowser_2 = new QTextBrowser(widget);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));

        verticalLayout_4->addWidget(textBrowser_2);


        horizontalLayout_11->addLayout(verticalLayout_4);

        layoutWidget->raise();
        layoutWidget->raise();
        label_4->raise();
        textBrowser->raise();
        textBrowser_2->raise();
        label_4->raise();
        label->raise();

        retranslateUi(deska);
        QObject::connect(pushButton_6, SIGNAL(clicked()), deska, SLOT(close()));

        QMetaObject::connectSlotsByName(deska);
    } // setupUi

    void retranslateUi(QWidget *deska)
    {
        deska->setWindowTitle(QApplication::translate("deska", "Form", 0));
        pushButton_2->setText(QApplication::translate("deska", "Next", 0));
        pushButton->setText(QApplication::translate("deska", "Prew", 0));
        pushButton_3->setText(QApplication::translate("deska", "Skip", 0));
        pushButton_4->setText(QApplication::translate("deska", "Save", 0));
        pushButton_5->setText(QApplication::translate("deska", "Menu", 0));
        pushButton_6->setText(QApplication::translate("deska", "Exit", 0));
        butt0_0->setText(QString());
        butt0_1->setText(QString());
        butt0_2->setText(QString());
        butt0_3->setText(QString());
        butt0_4->setText(QString());
        butt0_5->setText(QString());
        butt0_6->setText(QString());
        butt0_7->setText(QString());
        butt1_0->setText(QString());
        butt1_1->setText(QString());
        butt1_2->setText(QString());
        butt1_3->setText(QString());
        butt1_4->setText(QString());
        butt1_5->setText(QString());
        butt1_6->setText(QString());
        butt1_7->setText(QString());
        butt2_0->setText(QString());
        butt2_1->setText(QString());
        butt2_2->setText(QString());
        butt2_3->setText(QString());
        butt2_4->setText(QString());
        butt2_5->setText(QString());
        butt2_6->setText(QString());
        butt2_7->setText(QString());
        butt3_0->setText(QString());
        butt3_1->setText(QString());
        butt3_2->setText(QString());
        butt3_3->setText(QString());
        butt3_4->setText(QString());
        butt3_5->setText(QString());
        butt3_6->setText(QString());
        butt3_7->setText(QString());
        butt4_0->setText(QString());
        butt4_1->setText(QString());
        butt4_2->setText(QString());
        butt4_3->setText(QString());
        butt4_4->setText(QString());
        butt4_5->setText(QString());
        butt4_6->setText(QString());
        butt4_7->setText(QString());
        butt5_0->setText(QString());
        butt5_1->setText(QString());
        butt5_2->setText(QString());
        butt5_3->setText(QString());
        butt5_4->setText(QString());
        butt5_5->setText(QString());
        butt5_6->setText(QString());
        butt5_7->setText(QString());
        butt6_0->setText(QString());
        butt6_1->setText(QString());
        butt6_2->setText(QString());
        butt6_3->setText(QString());
        butt6_4->setText(QString());
        butt6_5->setText(QString());
        butt6_6->setText(QString());
        butt6_7->setText(QString());
        butt7_0->setText(QString());
        butt7_1->setText(QString());
        butt7_2->setText(QString());
        butt7_3->setText(QString());
        butt7_4->setText(QString());
        butt7_5->setText(QString());
        butt7_6->setText(QString());
        butt7_7->setText(QString());
        label->setText(QString());
        label_4->setText(QApplication::translate("deska", "<html><head/><body><p align=\"center\"><span style=\" font-weight:600; text-decoration: underline;\">\304\214ern\303\275</span></p></body></html>", 0));
        label_5->setText(QApplication::translate("deska", "<html><head/><body><p align=\"center\"><span style=\" font-weight:600; text-decoration: underline;\">B\303\255l\303\275</span></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class deska: public Ui_deska {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DESKA_H
