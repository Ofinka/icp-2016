/********************************************************************************
** Form generated from reading UI file 'loadgame.ui'
**
** Created: Thu May 5 23:37:17 2016
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOADGAME_H
#define UI_LOADGAME_H

#include <QtCore/QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QHeaderView>
#include <QListWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_loadGame
{
public:
    QListWidget *listWidget;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;

    void setupUi(QWidget *loadGame)
    {
        if (loadGame->objectName().isEmpty())
            loadGame->setObjectName(QString::fromUtf8("loadGame"));
        loadGame->resize(600, 400);
        listWidget = new QListWidget(loadGame);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(180, 50, 256, 192));
        widget = new QWidget(loadGame);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(260, 280, 77, 83));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(widget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout->addWidget(pushButton_3);


        retranslateUi(loadGame);
        QObject::connect(pushButton_3, SIGNAL(clicked()), loadGame, SLOT(close()));

        QMetaObject::connectSlotsByName(loadGame);
    } // setupUi

    void retranslateUi(QWidget *loadGame)
    {
        loadGame->setWindowTitle(QApplication::translate("loadGame", "Form", 0));
        pushButton->setText(QApplication::translate("loadGame", "Na\304\215\303\255st", 0));
        pushButton_2->setText(QApplication::translate("loadGame", "Zp\304\233t", 0));
        pushButton_3->setText(QApplication::translate("loadGame", "Exit", 0));
    } // retranslateUi

};

namespace Ui {
    class loadGame: public Ui_loadGame {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOADGAME_H
