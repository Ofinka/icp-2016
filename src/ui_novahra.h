/********************************************************************************
** Form generated from reading UI file 'novahra.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NOVAHRA_H
#define UI_NOVAHRA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_novaHra
{
public:
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QRadioButton *radioButton_7;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_6;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;

    void setupUi(QWidget *novaHra)
    {
        if (novaHra->objectName().isEmpty())
            novaHra->setObjectName(QStringLiteral("novaHra"));
        novaHra->resize(600, 400);
        layoutWidget = new QWidget(novaHra);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(130, 150, 442, 26));
        horizontalLayout_4 = new QHBoxLayout(layoutWidget);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        radioButton_7 = new QRadioButton(layoutWidget);
        radioButton_7->setObjectName(QStringLiteral("radioButton_7"));
        radioButton_7->setChecked(false);

        horizontalLayout_2->addWidget(radioButton_7);

        radioButton_4 = new QRadioButton(layoutWidget);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setChecked(true);
        radioButton_4->setAutoRepeat(false);
        radioButton_4->setAutoExclusive(true);

        horizontalLayout_2->addWidget(radioButton_4);

        radioButton_5 = new QRadioButton(layoutWidget);
        radioButton_5->setObjectName(QStringLiteral("radioButton_5"));
        radioButton_5->setChecked(false);

        horizontalLayout_2->addWidget(radioButton_5);

        radioButton_6 = new QRadioButton(layoutWidget);
        radioButton_6->setObjectName(QStringLiteral("radioButton_6"));

        horizontalLayout_2->addWidget(radioButton_6);


        horizontalLayout_4->addLayout(horizontalLayout_2);

        layoutWidget1 = new QWidget(novaHra);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(130, 90, 444, 50));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(layoutWidget1);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        radioButton = new QRadioButton(layoutWidget1);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setChecked(true);
        radioButton->setAutoExclusive(true);

        horizontalLayout->addWidget(radioButton);

        radioButton_2 = new QRadioButton(layoutWidget1);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));

        horizontalLayout->addWidget(radioButton_2);

        radioButton_3 = new QRadioButton(layoutWidget1);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));

        horizontalLayout->addWidget(radioButton_3);


        horizontalLayout_3->addLayout(horizontalLayout);


        verticalLayout->addLayout(horizontalLayout_3);

        layoutWidget2 = new QWidget(novaHra);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(280, 220, 87, 95));
        verticalLayout_2 = new QVBoxLayout(layoutWidget2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(layoutWidget2);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout_2->addWidget(pushButton);

        pushButton_2 = new QPushButton(layoutWidget2);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        verticalLayout_2->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(layoutWidget2);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        verticalLayout_2->addWidget(pushButton_3);


        retranslateUi(novaHra);
        QObject::connect(pushButton_3, SIGNAL(clicked()), novaHra, SLOT(close()));

        QMetaObject::connectSlotsByName(novaHra);
    } // setupUi

    void retranslateUi(QWidget *novaHra)
    {
        novaHra->setWindowTitle(QApplication::translate("novaHra", "Form", 0));
        label_2->setText(QApplication::translate("novaHra", "Velikost desky", 0));
        radioButton_7->setText(QApplication::translate("novaHra", "6", 0));
        radioButton_4->setText(QApplication::translate("novaHra", "8", 0));
        radioButton_5->setText(QApplication::translate("novaHra", "10", 0));
        radioButton_6->setText(QApplication::translate("novaHra", "12", 0));
        label->setText(QApplication::translate("novaHra", "Obt\303\255\305\276nost", 0));
        radioButton->setText(QApplication::translate("novaHra", "Lehk\303\241", 0));
        radioButton_2->setText(QApplication::translate("novaHra", "St\305\231edn\303\255", 0));
        radioButton_3->setText(QApplication::translate("novaHra", "Multiplayer", 0));
        pushButton->setText(QApplication::translate("novaHra", "Hr\303\241t", 0));
        pushButton_2->setText(QApplication::translate("novaHra", "Zp\304\233t", 0));
        pushButton_3->setText(QApplication::translate("novaHra", "Exit", 0));
    } // retranslateUi

};

namespace Ui {
    class novaHra: public Ui_novaHra {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NOVAHRA_H
